/*
 *	Steering daemon
 *  Reads steering message from VisIt, sends to the automatic tuning agent on the simulation side
 *
 *  Developed by Preeti AUG 21-08-2010
 */

#include "visclient.h"

using namespace std;

/*
 *	Handler for receiving messages from simulation daemon (simdaemon)
 *	This message is sent to VisIt
 */

void sim_msg_handler(int sig_num)
{
	static char func[] = "sim_msg_handler";

	fprintf (DBG, "visclient: %s: Received sig %d.\n", func, sig_num);

	// Receive from simulation daemon

	if((smsgsz=(recv(sockfd, smsgcontent, sizeof(smsgcontent), 0)))> 0) {
		smsgcontent[smsgsz]='\0';
		fprintf(DBG, "visclient: %s: Received message [%s] of %d bytes.\n", func, smsgcontent, smsgsz);
	}
	else {
		perror("visclient: recv error");
		//return;
	}

	printf("visclient: %s: Received message [%s]\n", func, smsgcontent);

}

/* 
 *	Open a socket connection with simulation site
 *	Let vis be client and sim be server
 */

void setup_socket()
{
		// Create an endpoint for communication

		if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
				perror("visclient: socket error ");
				exit(1);
		}

		// Fill socket structure with host information

		memset(&address, 0, sizeof(address));	//bzero(&address, sizeof(address));
		address.sin_family = AF_INET;
		address.sin_port = htons(PORT);
		address.sin_addr.s_addr = ((struct in_addr *)(hostname->h_addr))->s_addr;

		// Connect to socket 

		if (connect(sockfd, (struct sockaddr *)&address, sizeof(address)) == -1) {		
				perror("visclient: connect error ");
				exit(1);
		}

		// Signal handler to signal visclient when simdaemon sends message

		signal(SIGIO, sim_msg_handler);

		if (fcntl(sockfd, F_SETOWN, getpid()) == -1) {
			perror("visclient: F_SETOWN");
			exit(1);
		}
		if (fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFL, 0) | FASYNC) == -1) {
			perror("visclient: F_SETFL");
			exit(1);
		}


}

/* This handler will be called when the queue becomes non-empty. */

void handler (int sig_num) {
	
	// Send to simulation daemon simd
	write(sockfd, msgcontent, msgsz);

}

void cleanup()
{
		// close socket descriptor
		close(sockfd);

}


/*
 * Main program
 * 
 * Arguments
 * 
 * * Simulation site address 
 *
 */


int main(int argc, char *argv[]) {
        string userinput = "";
	int length;

	if(argc < 2)
	{
		cout << "Missing argument \nUsage: ./visclient <simulation-site-address>" << endl;
		exit(1);
	}

	DBG = fopen(DEBUGFILE, "w");

	// Get IP address for the host

	if ((hostname = gethostbyname(argv[1])) == 0) {
		perror("visclient: gethostbyname error ");
		exit(1);
	}


	setup_socket();
	cout << "Successfully connected to the Simulation server." << endl << endl;

	while(1)
	{
	  cout << "Please enter the following values ($formNest, $nestXPos, $nestYPos, $nestRes, $simRes, $OI, $Rate, $startDate) space delimited. For example 0 -1 -1 -1 -1 23 -1 2009-05-23_18:00:00" << endl << endl;
	  getline(cin, userinput);
	  
	  
	  if(userinput.length() > 0) {
	         length = userinput.copy(msgcontent, userinput.length(), 0 );
		 write(sockfd, msgcontent, length);
	  }
	 
	  sleep(60);
	  flush(cout);
	}

	fclose(DBG);

  cleanup();      
  return 0;
}


