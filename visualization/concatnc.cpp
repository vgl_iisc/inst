#include "concatnc.h"

using namespace std;

char *filename[MAX_FILES] = {NULL};

void Splitfiles::setValues(int ncid, char *filename)
{
	static char func[] = "Splitfiles::setValues";

	ncId = ncid;

	fileName = (char *) malloc (strlen(filename) * sizeof(char));
	strcpy(fileName, filename);

	status = 0;

	return ;
}

void Splitfiles::setVarValues(int var_id, nc_type var_type, char *var_name, int var_ndims, int *var_dimids)
{
	static char func[] = "Splitfiles::setVarValues";

	fileVar[var_id].varId = var_id;
	fileVar[var_id].varType = var_type;
	fileVar[var_id].varNdims = var_ndims;

	varcurrdimcount = (size_t *) malloc (var_ndims * sizeof(size_t));
	fileVar[var_id].dimCount = (size_t *) malloc (var_ndims * sizeof(size_t));
	fileVar[var_id].varDimids = (int *) malloc (var_ndims * sizeof(int) );

	//initialize
	for(int i=0;i<var_ndims;i++)
	{
		varcurrdimcount[i]=0;
		fileVar[var_id].dimCount[i] = dimcount[i];
		fileVar[var_id].varDimids[i] = dimension[var_dimids[i]].dimSize;
	}

	fileVar[var_id].varName = (char *) malloc (strlen(var_name) * sizeof(char));
	strcpy(fileVar[var_id].varName, var_name);

	return;
}

void Splitfiles::getVardimcounts(int var_id)
{
	for(int i=0;i<fileVar[var_id].varNdims;i++)
	{
		currdimcount[i]=fileVar[var_id].dimCount[i];
	}	
}

void Splitfiles::printValues(void)
{
	return ;
}

int Splitfiles::getId(void)
{
	return ncId;
}

char *Splitfiles::getFilename(void)
{
	return fileName;
}

void Variable::setValues(int var_id, nc_type var_type, char *var_name, int var_ndims, int *var_dimids)
{
	varId = var_id;
	varType = var_type;
	varNdims = var_ndims;

	varDimids = (int *) malloc (var_ndims * sizeof(int) );

	//initialize
	for(int i=0;i<var_ndims;i++)
	{
		varDimids[i] = var_dimids[i];
	}

	varName = (char *) malloc (strlen(var_name) * sizeof(char));
	strcpy(varName, var_name);

	charVarPtr = NULL;
	intVarPtr = NULL;
	floatVarPtr = NULL;

	oldSpace = 0;
	oldBlock = 0;

	return ;
}

void Variable::getVardimids(void)
{
	for(int i=0;i<varNdims;i++)
	{		
		currdimid[i]=varDimids[i];
	}
}

int Variable::getVarNdims(void)
{
	return varNdims;
}

nc_type Variable::getVarType(void)
{
	return varType;
}

char *Variable::getVarName(void)
{
	return varName;
}

int get_xydim(char *dir)
{
	static char func[] = "get_xydim";	

	if((dp  = opendir(dir)) == NULL) {
		cout << "Error(" << errno << ") opening " << dir << endl;
		return errno;
	}

	i=-1;
	//Find dimensions from a file
	//Assumption is that file chunks from processors have same dimensions

	dirp = readdir(dp);
	dirp = readdir(dp);
	dirp = readdir(dp);

	if(strcmp(dirp->d_name, ".") != 0 && strcmp(dirp->d_name, "..") != 0)
	{		
		char *ffile = (char *) malloc (strlen(dirp->d_name) * sizeof(char*));
		strcpy(ffile, dirp->d_name);

		int len = strlen(dir) + strlen(ffile);
		char file[len];
		strcpy(file, dir);
		strcat(file, ffile);

		//File open
		if((status = nc_open(file, NC_NOWRITE, &id)) != NC_NOERR)
		{ERR(status); return 0;}
		ncid = id;

		//Get information about the open netCDF dataset
		status = nc_inq(ncid, &nDims, &nVars, &nGlobalAtts, &unlimitedDimension);
		if(status != NC_NOERR)
		{ERR(status); return 0;}

		nc_inq_dimid(ncid, wedimname, &wedimid);
		nc_inq_dimlen(ncid, wedimid, &west_east_local);

		nc_inq_dimid(ncid, sndimname, &sndimid);
		nc_inq_dimlen(ncid, sndimid, &south_north_local);


		//get the value of global attributes WEST-EAST_GRID_DIMENSION and SOUTH-NORTH_GRID_DIMENSION 
		//these will help in forming the x and y processor decomposition

		char attname1[] = "WEST-EAST_GRID_DIMENSION";
		if((status = nc_get_att_int(ncid, NC_GLOBAL, attname1, &west_east_global)) != NC_NOERR)
			ERR(status);

		char attname2[] = "SOUTH-NORTH_GRID_DIMENSION";
		if((status = nc_get_att_int(ncid, NC_GLOBAL, attname2, &south_north_global)) != NC_NOERR)
			ERR(status);

		val = west_east_global*1.f/west_east_local;
		wedim = rint(val);
		val = south_north_global*1.f/south_north_local;
		sndim = rint(val);

		//inquire dimid 
		nc_inq_dimid(ncid, wedimname, &wedimid);
		nc_inq_dimid(ncid, wedimstagname, &westagdimid);
		nc_inq_dimid(ncid, sndimname, &sndimid);
		nc_inq_dimid(ncid, sndimstagname, &snstagdimid);

		if(ncid != INVALID_FILE_HANDLE)
		{
			nc_close(ncid);
			ncid = INVALID_FILE_HANDLE;
		}
		
		find_dims(dir);

		//File open
		if((status = nc_open(file, NC_NOWRITE, &ncid)) != NC_NOERR)
		{ERR(status); return 0;}		

		prepare_file();

		//File close
		if(ncid != INVALID_FILE_HANDLE)
		{
			nc_close(ncid);
			ncid = INVALID_FILE_HANDLE;
		}

	}

	closedir(dp);
	dp = NULL;

	return 1;
}

//function to find correct dimensions for each file x by y, iterate through 0 - wedim-1 and 0 - sndim-1
int find_dims(char *dir)
{
	static char func[] = "find_dims";
	char *cmd;
	char lscmd[] = "/bin/ls ";
	cmd = (char *) malloc( (strlen(dir)+strlen(lscmd)+1) * sizeof(char) );
	strcpy(cmd,"");
	strcat(cmd,lscmd);
	strcat(cmd,dir);
	cmd[strlen(cmd)]='\0';
	FILE *fp;
	char path[MAX_FILENAME_LEN];
	int filecount=-1;

	fp = popen(cmd, "r");
	if (fp == NULL) {
		printf("Failed to run popen\n" );
		exit;
	}
	

	//inquire dimid for west_east
	
	dimension[wedimid].dimSize = 0;	//initialize
	dimension[wedimid].local_dimSize = (size_t *) malloc (wedim * sizeof(size_t));
	
	dimension[westagdimid].dimSize = 0;	//initialize
	dimension[westagdimid].local_dimSize = (size_t *) malloc (wedim * sizeof(size_t));	

	int count = -1;	
		
	while (fgets(path, sizeof(path)-1, fp) != NULL) {

		if(++count >= wedim)
			break;

		char *file = path;

		file[strlen(path)-1] = '\0'; 	//chomp the newline

		int len = strlen(dir) + strlen(file);
		char currfile[len];
		strcpy(currfile, dir);
		strcat(currfile, file);

		//File open
		if((status = nc_open(currfile, NC_NOWRITE, &ncid)) != NC_NOERR)
		{ERR(status); return 0;}

		for(int i = 0; i < nDims; ++i)
		{
			if((status = nc_inq_dim(ncid, i, dimName, &dimSize)) != NC_NOERR)
			{
				ERR(status);
				break;
			}
			nc_inq_dimid(ncid, dimName, &dimid);

			if(strcmp(dimName, wedimname) == 0)
			{
				dimension[dimid].dimSize += dimSize;
				dimension[dimid].local_dimSize[count] = dimension[dimid].dimSize;
			}
			else if(strcmp(dimName, wedimstagname) == 0)
			{
				dimension[dimid].dimSize += dimSize;
				dimension[dimid].local_dimSize[count] = dimension[dimid].dimSize;
			}
			else
				dimension[dimid].dimSize = dimSize;

		}

		nc_close(ncid);
	}
	
	// close 	
	pclose(fp);
	
	west_east_global = dimension[wedimid].dimSize;
	west_east_stag = dimension[westagdimid].dimSize;

	dimension[sndimid].dimSize = 0;	//initialize
	dimension[sndimid].local_dimSize = (size_t *) malloc (sndim * sizeof(size_t));

	dimension[snstagdimid].dimSize = 0;	//initialize
	dimension[snstagdimid].local_dimSize = (size_t *) malloc (sndim * sizeof(size_t));


	fp = popen(cmd, "r");
	if (fp == NULL) {
		printf("Failed to run popen\n" );
		exit;
	}
	
	count = -1;
		
	int sndimcount = -1;
	while (fgets(path, sizeof(path)-1, fp) != NULL) {

		if( ((++count)%wedim) > 0 )
			continue;

		++sndimcount;

		char *file = path;
		file[strlen(path)-1] = '\0'; 	//chomp the newline

		int len = strlen(dir) + strlen(file);
		char currfile[len];
		strcpy(currfile, dir);
		strcat(currfile, file);

		//File open
		if((status = nc_open(currfile, NC_NOWRITE, &ncid)) != NC_NOERR)
		{ERR(status); return 0;}

		for(int i = 0; i < nDims; ++i)
		{
			if((status = nc_inq_dim(ncid, i, dimName, &dimSize)) != NC_NOERR)
			{
				ERR(status);
				break;
			}
			nc_inq_dimid(ncid, dimName, &dimid);

			if(strcmp(dimName, sndimname) == 0)
			{
				dimension[dimid].dimSize += dimSize;
				dimension[sndimid].local_dimSize[sndimcount] = dimension[dimid].dimSize;
			}
			else if(strcmp(dimName, sndimstagname) == 0)
			{
				dimension[dimid].dimSize += dimSize;
				dimension[dimid].local_dimSize[sndimcount] = dimension[dimid].dimSize;
			}
		}

		nc_close(ncid);
	}
	
	// close 	
	pclose(fp);
	
	south_north_global = dimension[sndimid].dimSize;
	south_north_stag = dimension[snstagdimid].dimSize;
}

/*Define dimensions, variables, variable attributes, global attributes .. */

int prepare_file(void)
{
	int retval;

	/* Create the file. */
	if ((status = nc_create(OFILE_NAME, NC_CLOBBER, &o_ncid)))	 // create netCDF dataset: enter define mode 
		ERR(status);

	variable = (Variable *) malloc (nVars * sizeof(Variable));

	/*Define dimensions*/ 

	for(i = 0; i < nDims; ++i)
	{
		
		if((status = nc_inq_dim(ncid, i, dimName, &dimSize)) != NC_NOERR)
		{
			ERR(status);
			break;
		}

		nc_inq_dimid(ncid, dimName, &dimid);
		dimension[dimid].localdimSize = dimSize;
		
		dimSize = dimension[dimid].dimSize;

		if( strcmp(dimName, wedimstagname) == 0)		
		{
			dimension[dimid].localdimSize = west_east_local;
		}
		else if( strcmp(dimName, sndimstagname) == 0 )
		{
			dimSize = south_north_global + 1; 
			dimension[dimid].dimSize = dimSize;
			south_north_stag = south_north_global + 1;
			// the local dimension of south_north_stag is either x for middle files, and x+1 for end files
			dimension[dimid].localdimSize = south_north_local; //if the file picked up is the last so it has staggered dim, so this is workaround OR you can always pick up *0000*
		}
		
		//Define for the new file
		if ((status = nc_def_dim(o_ncid, dimName, dimSize, &dimid)) != NC_NOERR)
			ERR(status);

		strcpy(dimension[dimid].dimName, dimName);
	}


	/*Define variables*/
	for(varid = 0; varid < nVars; ++varid)
	{
		if((status = nc_inq_var(ncid, varid, varname, &vartype, &varndims, vardimids, &varnatts)) != NC_NOERR)
		{
			ERR(status);
			break;
		}

		//initialize variable structure

		variable[varid].setValues(varid, vartype, varname, varndims, vardimids);

		//Dimension of variables same
		if ((status = nc_def_var(o_ncid, varname, vartype, varndims, vardimids, &varid)) != NC_NOERR)
			ERR(status);

		
		for (int j=0;j<varnatts;j++)
		{	
			/* get attribute names */

			nc_inq_attname(ncid, varid, j, attname);

			/* get attribute types and lengths */

			if((status = nc_inq_att(ncid, varid, attname, &atttype, &attlen)) != NC_NOERR)
				ERR(status);


			if (atttype == NC_CHAR)
			{
				atttext = (char *) malloc((attlen+1) * sizeof(char *));

				if ((retval = nc_get_att_text(ncid, varid, attname, atttext)))
					ERR(retval);

				atttext[attlen]='\0';
					
				
				if ((status = nc_put_att_text(o_ncid, varid, attname, attlen, atttext)) != NC_NOERR)
					ERR(status);

				free(atttext);
				atttext = NULL;

			}
			else if (atttype == NC_INT)
			{
				int arr;
				status = nc_get_att_int(ncid, varid, attname, &arr);
				if(status != NC_NOERR)
				{
					ERR(status);
				}
				if ((status = nc_put_att_int(o_ncid, varid, attname, atttype, attlen, &arr)) != NC_NOERR)
					ERR(status);
			}
			else if (atttype == NC_FLOAT)
			{
				float arr;
				status = nc_get_att_float(ncid, varid, attname, &arr);
				if(status != NC_NOERR)
				{
					ERR(status);
				}
				if ((status = nc_put_att_float(o_ncid, varid, attname, atttype, attlen, &arr)) != NC_NOERR)
					ERR(status);
			}
			else if (atttype == NC_DOUBLE)
			{
				double arr;
				status = nc_get_att_double(ncid, varid, attname, &arr);
				if(status != NC_NOERR)
					ERR(status);
				if ((status = nc_put_att_double(o_ncid, varid, attname, atttype, attlen, &arr)) != NC_NOERR)
					ERR(status);
			}
		}	//end for loop - attributes
	}

	attlen = 0;
	atttype = (nc_type)0;
	free(atttext);

	atttext = NULL;

	/*Define global attributes*/
	for(i=0;i<nGlobalAtts;i++)
	{
		if((status = nc_inq_attname(ncid, NC_GLOBAL, i, attname)) != NC_NOERR)
			ERR(status);
		if((status = nc_inq_att(ncid, NC_GLOBAL, attname, &atttype, &attlen)) != NC_NOERR)
			ERR(status);

		if (atttype == NC_CHAR)
		{
			char attbuf[2048];

			if ( attlen < sizeof(attbuf) )
			{
				nc_get_att_text(ncid, NC_GLOBAL, attname, attbuf);
				attbuf[attlen++] = 0;
				if ( (int) attlen > attlen ) attlen = attlen;
				{
					atttext = (char *) malloc(attlen * sizeof(char *));
					memcpy(atttext, attbuf, attlen);
				}
			}
			else
			{
				atttext[0] = 0;
			}

			if ((status = nc_put_att_text(o_ncid, NC_GLOBAL, attname, strlen(atttext), atttext)) != NC_NOERR)
				ERR(status);
		}
		else if ( atttype == NC_INT || atttype == NC_SHORT )
		{
			int arr;
			status = nc_get_att_int(ncid, NC_GLOBAL, attname, &arr);
			if(status != NC_NOERR)
			{
				ERR(status);
			}

			if( strstr(attname, "PATCH_END") != NULL)
			{
				
				if(strcmp(attname, "WEST-EAST_PATCH_END_UNSTAG") ==0)
				{
					arr = west_east_global;
				}
				else if(strcmp(attname, "WEST-EAST_PATCH_END_STAG") ==0)
				{
					arr = west_east_stag;
				}
				else if(strcmp(attname, "SOUTH-NORTH_PATCH_END_UNSTAG") ==0)
				{
					arr = south_north_global;
				}
				else if(strcmp(attname, "SOUTH-NORTH_PATCH_END_STAG") ==0)
				{
					arr = south_north_stag;
				}
				
			}
			else if( strstr(attname, "PATCH_START") != NULL)
			{
				arr = 1;
			}


			if ((status = nc_put_att_int(o_ncid, NC_GLOBAL, attname, atttype, attlen, &arr)) != NC_NOERR)
				ERR(status);
		}
		else if ( atttype== NC_FLOAT || atttype == NC_DOUBLE )
		{
			double arr;
			status = nc_get_att_double(ncid, NC_GLOBAL, attname, &arr);
			if(status != NC_NOERR)
				ERR(status);
			if ((status = nc_put_att_double(o_ncid, NC_GLOBAL, attname, atttype, attlen, &arr)) != NC_NOERR)
				ERR(status);
		}
	}

	free(atttext);

	// End define mode
	if ((status = nc_enddef(o_ncid)))
		ERR(status);
	return 1;
}

/*Merge files*/

int get_files(char *dir)
{
	static char func[] = "get_files";

	char *cmd;
	char lscmd[] = "/bin/ls ";
	cmd = (char *) malloc( (strlen(dir)+strlen(lscmd)+1) * sizeof(char) );
	strcpy(cmd,"");
	strcat(cmd,lscmd);
	strcat(cmd,dir);
	cmd[strlen(cmd)]='\0';

	FILE *fp;
	//int status;
	char path[MAX_FILENAME_LEN];
	int filecount=-1;

	//Allocate space for variables

	fp = popen(cmd, "r");
	if (fp == NULL) {
		printf("Failed to run popen\n" );
		exit;
	}

	while (fgets(path, sizeof(path)-1, fp) != NULL) {

		filename[++filecount] = (char *) malloc (strlen(path) * sizeof(char));
		strcpy(filename[filecount], path);
		filename[filecount][strlen(path)-1] = '\0'; 	//chomp the newline

		int len = strlen(dir) + strlen(filename[filecount]);
		char file[len];
		strcpy(file, dir);
		strcat(file, filename[filecount]);

		//File open
		if((status = nc_open(file, NC_NOWRITE, &ncid)) != NC_NOERR)
		{ERR(status); return 0;}

		splitfiles[filecount].setValues(ncid, file);

		for(varid = 0; varid < nVars; ++varid)
		{
			if((status = nc_inq_var(ncid, varid, varname, &vartype, &varndims, vardimids, &varnatts)) != NC_NOERR)
			{
				ERR(status);
				break;
			}

			dimcount = (size_t *) malloc (varndims * sizeof(size_t) );
			for (int j=0;j<varndims;j++) {

				if((status = nc_inq_dimlen(ncid, vardimids[j], &dimcount[j])) != NC_NOERR)
					ERR(status);

				num_vals *= dimcount[j];
			}

			splitfiles[filecount].setVarValues(varid, vartype, varname, varndims, vardimids);

			free(dimcount);
		}

	}//end while

	for (i=0;i<=filecount;i++)
		splitfiles[i].printValues();

	totalfiles = filecount+1;

	// close 	
	pclose(fp);

}


int merge_files(void)
{
	for(varid = 0; varid < nVars; ++varid)
	{		
		int fpos_x, fpos_y;

		varndims = variable[varid].getVarNdims();
		
		size_t *readstart = (size_t *) malloc (varndims * sizeof(size_t));
		if(readstart == NULL)
			cout <<"readstart null\n";
		size_t *writestart = (size_t *) malloc (varndims * sizeof(size_t));
		if(writestart == NULL)
			cout <<"writestart null\n";

		// 4 dimensions
		if(varndims == 4)
		{
			currdimid = (int *) malloc (varndims * sizeof(int));

			variable[varid].getVardimids(); //will be populated in currdimid

			for(int dim0=0 ; dim0 < dimension[currdimid[0]].dimSize ; dim0 ++)
			{
				for(int dim1=0 ; dim1 < dimension[currdimid[1]].dimSize ; dim1 ++)
				{
					for(int dim2=0 ; dim2 < dimension[currdimid[2]].dimSize ; dim2 ++)
					{
						for(int dim3=0 ; dim3 < dimension[currdimid[3]].dimSize;)
						{
							currdimcount= (size_t *) malloc (varndims * sizeof(size_t) );

							//global position  dim1, dim2, dim3, dim4
							

							//get the file number
							fpos_x = dim3 / dimension[currdimid[3]].localdimSize;
							fpos_y = dim2 / dimension[currdimid[2]].localdimSize;


							if( strcmp(dimension[currdimid[3]].dimName, wedimname) >= 0)
							{
								for (int i=0;i<wedim;i++)
								{
									if(dim3 < dimension[currdimid[3]].local_dimSize[i])
										{fpos_x = i;break;}
								}
							}
							else
								printf("\nsomething different %s\n",dimension[currdimid[3]].dimName);

							if( strcmp(dimension[currdimid[2]].dimName, sndimname) >= 0)
							{
								for (int i=0;i<sndim;i++)
								{
									if(dim2 < dimension[currdimid[2]].local_dimSize[i])
										{fpos_y = i;break;}
						
								}
							}
							else
								printf("\nsomething different %s\n",dimension[currdimid[2]].dimName);




							fpos_x = fpos_x > wedim-1 ? wedim-1 : fpos_x;
							fpos_y = fpos_y > sndim-1 ? sndim-1 : fpos_y;

							

							int filenum = fpos_y * wedim + fpos_x;
							int ncid = splitfiles[filenum].getId();

							splitfiles[filenum].getVardimcounts(varid);
							int currdim3 = currdimcount[3];


							free(currdimcount);
							currdimcount= (size_t *) malloc (varndims * sizeof(size_t));
							if(currdimcount == NULL)
								cout <<"currdimcount null\n";

							//get local position in file from global position

							int xsub=0, ysub=0, fileNo;
							for(int xdir=fpos_x-1;xdir>=0;xdir--)
							{
								fileNo = fpos_y * wedim + xdir;
								splitfiles[fileNo].getVardimcounts(varid);	//currdimcount populated
								xsub += currdimcount[3];							
							}
							for(int ydir=fpos_y-1;ydir>=0;ydir--)
							{
								fileNo = ydir * wedim + fpos_x;		//this is always the definition of file number
								splitfiles[fileNo].getVardimcounts(varid);	//currdimcount populated
								ysub += currdimcount[2];
							}

							int startx = dim3 - xsub;
							int starty = dim2 - ysub;

							readstart[0]=dim0;
							readstart[1]=dim1;
							readstart[2]=starty; 
							readstart[3]=startx;
							size_t readcount[] = {1,1,1,currdim3};	

							writestart[0]=dim0; 
							writestart[1]=dim1;
							writestart[2]=dim2; 
							writestart[3]=dim3;
							size_t writecount[] = {1,1,1,currdim3};


							vartype = variable[varid].getVarType();

							if(vartype == NC_CHAR)	{
								char *array;
								array = (char *) malloc (currdim3 * sizeof(char));
								if((status = nc_get_vara_text(ncid, varid, readstart, readcount, array)) != NC_NOERR)
									ERR(status);
								if((status = nc_put_vara_text(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
									ERR(status);
								free(array);
							}
							else 
								if(vartype == NC_INT || vartype == NC_SHORT) {
									int *array;
									array = (int *) malloc (currdim3 * sizeof(int));

									if((status = nc_get_vara_int(ncid, varid, readstart, readcount, array)) != NC_NOERR)
										ERR(status);

									if((status = nc_put_vara_int(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
										ERR(status);
									free(array);
								}
								else
									if(vartype == NC_FLOAT)
									{
										float *array ;
										array = (float *) malloc (currdim3 * sizeof(float));

										if((status = nc_get_vara_float(ncid, varid, readstart, readcount, array)) != NC_NOERR)
											ERR(status);

										if((status = nc_put_vara_float(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
											ERR(status);
										free(array);
									}

							free(currdimcount);

							dim3 += currdim3;

						}

					}

				}

			}

			free(currdimid);
			free(readstart);
			free(writestart);
		}




		// 3 dimensions
		else if(varndims == 3)
		{
			int fpos_x, fpos_y;

			currdimid = (int *) malloc (varndims * sizeof(int));
			variable[varid].getVardimids(); //will be populated in currdimid

			size_t *readstart = (size_t *) malloc (varndims * sizeof(size_t));
			if(readstart == NULL)
				cout <<"readstart null\n";
			size_t *writestart = (size_t *) malloc (varndims * sizeof(size_t));
			if(writestart == NULL)
				cout <<"writestart null\n";

			for(int dim0=0 ; dim0 < dimension[currdimid[0]].dimSize ; dim0 ++)
			{
				for(int dim1=0 ; dim1 < dimension[currdimid[1]].dimSize ; dim1 ++)
				{
					for(int dim2=0 ; dim2 < dimension[currdimid[2]].dimSize ; )
					{
						currdimcount= (size_t *) malloc (varndims * sizeof(size_t) );

						//global position  dim1, dim2, dim3, dim4

						//get the file number
						fpos_x = dim2 / dimension[currdimid[2]].localdimSize;
						fpos_y = dim1 / dimension[currdimid[1]].localdimSize;

						if( strcmp(dimension[currdimid[2]].dimName, wedimname) >= 0)
						{
							for (int i=0;i<wedim;i++)
							{
								if(dim2 < dimension[currdimid[2]].local_dimSize[i])
									{
										fpos_x = i;
										break;
									}
							}
						}
						else
							printf("\nsomething different %s\n",dimension[currdimid[2]].dimName);

						if( strcmp(dimension[currdimid[1]].dimName, sndimname) >= 0)
						{
							for (int i=0;i<sndim;i++)
							{
								if(dim1 < dimension[currdimid[1]].local_dimSize[i])
									{
										fpos_y = i;
										break;
									}
					
							}
						}
						else
							printf("\nsomething different %s\n",dimension[currdimid[1]].dimName);


						fpos_x = fpos_x > wedim-1 ? wedim-1 : fpos_x;	//x corresponds to column
						fpos_y = fpos_y > sndim-1 ? sndim-1 : fpos_y;

						int filenum = fpos_y * wedim + fpos_x;
						int ncid = splitfiles[filenum].getId();

						splitfiles[filenum].getVardimcounts(varid);	// fills currdimcount
					
						int currdim2 = currdimcount[2];
						free(currdimcount);
						currdimcount= (size_t *) malloc (varndims * sizeof(size_t));

						if(currdimcount == NULL)
							cout <<"currdimcount null\n";

						//get local position in file from global position

						int xsub=0, ysub=0, fileNo;
						for(int xdir=fpos_x-1;xdir>=0;xdir--)
						{
							fileNo = fpos_y * wedim + xdir;
							splitfiles[fileNo].getVardimcounts(varid); //currdimcount populated
							xsub += currdimcount[2];	
						}

						free(currdimcount);
						currdimcount= (size_t *) malloc (varndims * sizeof(size_t));
						for(int ydir=fpos_y-1;ydir>=0;ydir--)
						{
							fileNo = ydir * wedim + fpos_x;		//this is always the definition of file number
							splitfiles[fileNo].getVardimcounts(varid); //currdimcount populated
							ysub += currdimcount[1];	
						}

						int startx = dim2 - xsub;
						int starty = dim1 - ysub;
						
						readstart[0]=dim0;
						readstart[1]=starty;
						readstart[2]=startx; 
						size_t readcount[] = {1,1,currdim2};

						writestart[0]=dim0; 
						writestart[1]=dim1;
						writestart[2]=dim2; 
						size_t writecount[] = {1,1,currdim2};

						vartype = variable[varid].getVarType();

						if(vartype == NC_CHAR)	{
							char *array;
							array = (char *) malloc (currdim2 * sizeof(char));
							if((status = nc_get_vara_text(ncid, varid, readstart, readcount, array)) != NC_NOERR)
								ERR(status);
							if((status = nc_put_vara_text(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
								ERR(status);
							free(array);
						}
						else 
							if(vartype == NC_INT || vartype == NC_SHORT) {
								int *array;
								array = (int *) malloc (currdim2 * sizeof(int));
								if((status = nc_get_vara_int(ncid, varid, readstart, readcount, array)) != NC_NOERR)
									ERR(status);
								if((status = nc_put_vara_int(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
									ERR(status);
								free(array);
							}
							else
								if(vartype == NC_FLOAT)
								{
									float *array ;
									array = (float *) malloc (currdim2 * sizeof(float));
									if((status = nc_get_vara_float(ncid, varid, readstart, readcount, array)) != NC_NOERR)
										ERR(status);
									if((status = nc_put_vara_float(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
										ERR(status);
									free(array);
								}

						
						free(currdimcount);
						dim2 += currdim2;
					}
				}
			}

			free(currdimid);
			free(readstart);
			free(writestart);

		}



		// 2 dimensions
		else if(varndims == 2)
		{
			int currdim1;
			currdimid = (int *) malloc (varndims * sizeof(int));
			variable[varid].getVardimids(); //will be populated in currdimid

			size_t *readstart = (size_t *) malloc (varndims * sizeof(size_t));
			if(readstart == NULL)
				cout <<"readstart null\n";
			size_t *writestart = (size_t *) malloc (varndims * sizeof(size_t));
			if(writestart == NULL)
				cout <<"writestart null\n";

			for(int dim0=0 ; dim0 < dimension[currdimid[0]].dimSize ; dim0 ++)
			{
				int dim1 = 0;

				for(int fileno=0; fileno < 1 ;fileno++)		//apparently same values in all files? VERIFY TODO
				{
					currdimcount= (size_t *) malloc (varndims * sizeof(size_t) );
					splitfiles[fileno].getVardimcounts(varid);	// fills currdimcount

					currdim1=currdimcount[1];
					
					readstart[0]=dim0;
					readstart[1]=0;
					size_t readcount[] = {1,currdim1};

					writestart[0]=dim0; 
					writestart[1]=dim1;
					size_t writecount[] = {1,currdim1};

					vartype = variable[varid].getVarType();

					if(vartype == NC_CHAR)	{
						char *array;
						array = (char *) malloc (currdim1 * sizeof(char));
						if((status = nc_get_vara_text(ncid, varid, readstart, readcount, array)) != NC_NOERR)
							ERR(status);
						if((status = nc_put_vara_text(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
							ERR(status);
						free(array);
					}
					else 
						if(vartype == NC_INT || vartype == NC_SHORT) {
							int *array;
							array = (int *) malloc (currdim1 * sizeof(int));
							if((status = nc_get_vara_int(ncid, varid, readstart, readcount, array)) != NC_NOERR)
								ERR(status);
							if((status = nc_put_vara_int(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
								ERR(status);
							free(array);
						}
						else
							if(vartype == NC_FLOAT)
							{
								float *array ;
								array = (float *) malloc (currdim1* sizeof(float));
								if((status = nc_get_vara_float(ncid, varid, readstart, readcount, array)) != NC_NOERR)
									ERR(status);
								if((status = nc_put_vara_float(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
									ERR(status);
								free(array);
							}

					dim1 += currdimcount[1];
					free(currdimcount);
					
				}

			}

			free(currdimid);
			free(readstart);
			free(writestart);

		}

		// 1 dimension
		else
		{
			currdimid = (int *) malloc (varndims * sizeof(int));
			variable[varid].getVardimids(); //will be populated in currdimid

			size_t *readstart = (size_t *) malloc (varndims * sizeof(size_t));
			if(readstart == NULL)
				cout <<"readstart null\n";
			size_t *writestart = (size_t *) malloc (varndims * sizeof(size_t));
			if(writestart == NULL)
				cout <<"writestart null\n";

			int fileno=0;	//taking the first file
			currdimcount= (size_t *) malloc (varndims * sizeof(size_t) );
			splitfiles[fileno].getVardimcounts(varid);	// fills currdimcount

			int currdim0=currdimcount[0];
			
			readstart[0]=0;
			size_t readcount[] = {currdim0};

			writestart[0]=0;
			size_t writecount[] = {currdim0};

			vartype = variable[varid].getVarType();

			if(vartype == NC_CHAR)	{
				char *array;
				array = (char *) malloc (currdim0 * sizeof(char));
				if((status = nc_get_vara_text(ncid, varid, readstart, readcount, array)) != NC_NOERR)
					ERR(status);
				if((status = nc_put_vara_text(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
					ERR(status);
				free(array);
			}
			else 
				if(vartype == NC_INT || vartype == NC_SHORT) {
					int *array;
					array = (int *) malloc (currdim0 * sizeof(int));
					if((status = nc_get_vara_int(ncid, varid, readstart, readcount, array)) != NC_NOERR)
						ERR(status);
					if((status = nc_put_vara_int(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
						ERR(status);
					free(array);
				}
				else
					if(vartype == NC_FLOAT)
					{
						float *array ;
						array = (float *) malloc (currdim0* sizeof(float));
						if((status = nc_get_vara_float(ncid, varid, readstart, readcount, array)) != NC_NOERR)
							ERR(status);
						if((status = nc_put_vara_float(o_ncid, varid, writestart, writecount, array)) != NC_NOERR)
							ERR(status);
						free(array);
					}
			
			free(currdimcount);
			free(currdimid);
			free(readstart);
			free(writestart);

		}
	}
	return 1;
}

int main(int argc, char **argv)
{ 	
	if(argc >= 3)
	{
		path = argv[1];
		OFILE_NAME = argv[2];

		get_xydim(path);	//calls prepare_file();
		get_files(path);
		merge_files();

		if(o_ncid != INVALID_FILE_HANDLE)
		{
			nc_close(o_ncid);
			o_ncid = INVALID_FILE_HANDLE;
		}

	}

	return 0;
}
