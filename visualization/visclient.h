#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>

/* name of the POSIX object referencing the queue for receiving from VisIt */
#define MSGSQOBJ_NAME    "/visit2visd"

/* name of the POSIX object referencing the queue for receiving from VisIt */
#define MSGRQOBJ_NAME    "/visd2visit"

/* max length of a message (just for this process) */
//#define MAX_MSG_LEN 2048
const int MAX_MSG_LEN = 10000;

FILE *DBG;
const char DEBUGFILE[] = "visdbg";

void setup_socket();
void handler(int);
void setup_mqueues();
void cleanup();

//Message queues	[with VisIt]

mqd_t rmsgq_id, smsgq_id;
char msgcontent[MAX_MSG_LEN], smsgcontent[MAX_MSG_LEN];
ssize_t msgsz, smsgsz;
unsigned int sender;
unsigned int msgprio = 20;	//for sending to VisIt
struct mq_attr msgq_attr;
struct sigevent sigevent;        /* For notification*/


//Socket connection		[with simd]

int sockfd, address_len, result;
struct sockaddr_in address;
struct hostent *hostname;

const int PORT=6666;

