#include <netcdf.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <math.h>
#include <iostream>

#define INVALID_FILE_HANDLE -1
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); return 2;}

#define MAX_FILENAME_LEN 80
//#define OFILE_NAME "merged.nc"

const int MAX_FILES = 200;
const int MAX_NUM_VARS = 200;

//char OFILE_NAME[] = "merged.nc"

const char* wedimname = "west_east";
const char* sndimname = "south_north";

const char* wedimstagname = "west_east_stag";
const char* sndimstagname = "south_north_stag";

int wedimid, sndimid, westagdimid, snstagdimid;

size_t west_east, west_east_stag, south_north, south_north_stag;

char *path, *OFILE_NAME, *FILE0000;
//vector<string> files;
//char *files[MAX_FILES];

DIR *dp;
struct dirent *dirp;

int id = INVALID_FILE_HANDLE;
int nDims, nVars, nGlobalAtts, unlimitedDimension;
int status, ncid, o_ncid, ncfileid[MAX_FILES];

int dimid;	
char dimName[50];
size_t dimSize;

char varname[NC_MAX_NAME+1];
nc_type vartype;
int varndims, varid, varnatts;
int vardimids[NC_MAX_VAR_DIMS];
size_t dimlen[NC_MAX_VAR_DIMS];
size_t *dimcount, *currdimcount;
int *currdimid;

int attnum;
char attname[NC_MAX_NAME+1];
nc_type atttype;
size_t attlen;
char *atttext;

int i,j,num_vals, totalfiles;
int west_east_global, south_north_global, wedim, sndim;
size_t west_east_local, south_north_local;
float val;

int get_xydim(char *);
int find_dims(char *);
int prepare_file(void);
int get_files(char *);

struct Dim
{
	char dimName[NC_MAX_NAME];
	size_t *local_dimSize;
	size_t dimSize;
	size_t localdimSize;
};

Dim dimension[NC_MAX_VAR_DIMS];

class Splitfiles {

   private:
	int ncId;
	char *fileName;
	size_t *varcurrdimcount;
	
	int status;

	struct Var {
		int varId;
		nc_type varType;
		char *varName;
		int varNdims;
		int *varDimids;
		size_t *dimCount;
	};

	Var fileVar[MAX_NUM_VARS];

   public:
	void setVarValues(int, nc_type, char*, int, int*);
	void setValues(int, char*);
	void getVardimcounts(int);
	void printValues(void);
	int getId(void);
	char *getFilename(void);
	void getDimSizes(int);
	~Splitfiles(){}
};

Splitfiles splitfiles[MAX_FILES];

void addVariable(int, int, int, void*);

class Variable {

   private:

	int varId;
	nc_type varType;
	char *varName;
	int varNdims;
	int *varDimids;
	size_t *start, *vardimcount;
	char *charVarPtr;//void *varPtr;
	int *intVarPtr;
	float *floatVarPtr;
	int oldSpace, oldBlock;

   public:

	Variable(){}
	void setValues(int, nc_type, char*, int, int*);
	void setVardimcount(void);
	int getVarNdims(void);
	void getVardimcount(void);
	void getVardimids(void);
	nc_type getVarType(void);
	char* getVarName(void);
	~Variable(){}

};

Variable *variable;

