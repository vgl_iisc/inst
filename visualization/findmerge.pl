#!/usr/bin/perl
#This program finds when all split files for a timestamp are there in a folder

use Cwd;

#**** PATHS to be set ****

my $NCDIR = '/home/user/run2'; #the directory where files are copied from the simulation server

my $VISITDIR = '/home/user/visit-tests/test/'; #the directory where split netCDF files are copied for the executable 'concatnc' to merge them

my $FILESDIR = '/home/user/visit-tests/run2/concat/'; #the directory where files are dropped after merging the split netCDF files.
#****


our ($currtime, $timestring);
our (@files_d01, @files_d02);

our ($resumetransfer, @timeread, $timestampread);
my $resumetransfer = 'resumetransfer';

our $cfgfilename = 'simdcfg';
open (OUT, ">> $cfgfilename ") || die "$cfgfilename open failed: $!\n";
our $debugfile = 'debugfile';
if(-e $debugfile)
{	
	`mv $debugfile debugfileold`;
}
open (DBGFILE, "> $debugfile ") || die "$debugfile open failed: $!\n";

while(1)
{
	if(-e 'donottransfer')
	{
		`rm donottransfer`;

		while(1)
		{
			sleep 3;
			if(-e 'resumetransfer')
			{
				open(FH,  "< $resumetransfer ") || die "$resumetransfer open failed: $!\n";
				@timeread = <FH>;
				close(FH);

				&discard_rest();

				`rm resumetransfer`;
				last;
			}
		}
	}
	&listfiles();
}

close OUT;


sub discard_rest
{
        chomp($timeread[0]);

        $timeread[0] =~ s/:/_/g;

        $timestamp = $timeread[0];
		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
        print "$currtime: time read $timestamp\n";

        my $files =  "d01_";
        &delete($files);
        my $files =  "d02_";
        &delete($files);

}


sub delete
{
        my $files = $_[0];
        my $cmd = "ls $NCDIR/$files* 2>/dev/null";
        my @delfiles = `$cmd`;
        my $i;
        my $tomatch = "$files$timestamp";

        print "$tomatch\nloop start\n";

        for($i = 0; $i <= $#delfiles ; $i++)
        {
                chomp($delfiles[$i]);
                my $subdelfiles = substr($delfiles[$i], 18, 23);

                if($subdelfiles ge $tomatch)
                {
                        `rm $delfiles[$i]`;
                        print "$delfiles[$i]\n";
                }
        }
        print "\n";
}





sub listfiles
{

		my @file_d01 = `for i in d01*0000*gz ; do ls $NCDIR/\$i ;done 2> /dev/null`;

		if ($#file_d01 < 0)
		{
				print "1";
				sleep(1);
				return;
		}

		if ($#file_d01 > 0 )	#ie atleast 2 timesteps present
		{
			#more than 1 file of 0000 is present => the older file is completely transferred assumedly
			#so now check the timestamp of the oldest file
			my $startindex = length($NCDIR)+1;
			chomp($file_d01[0]);
			my $substr1 = substr($file_d01[0], $startindex, 23);		
			@files_d01 = `for i in $substr1*gz ; do ls $NCDIR/\$i ;done 2> /dev/null`;

			$timestring = substr($substr1, 4);
			my $filenum = $#files_d01 + 1;
			print OUT "$timestring $filenum\n";
			
			#call to merge, delete
			&merge_d01('d01_');

			my $timestamp = substr($substr1, 4, 16);			@files_d02 = `for i in d02*$timestamp*gz ; do ls $NCDIR/\$i ;done 2> /dev/null`;
	
			my $flag = 1;
			while($#files_d02 < 0)
			{
				sleep(1);
				@files_d02 = `for i in *$timestamp*gz ; do ls $NCDIR/\$i ;done 2> /dev/null`;

				@file_d01 = `for i in d01*0000*gz ; do ls $NCDIR/\$i ;done 2> /dev/null`;
				if ($#file_d01 >= 0)
				{
						print @files_d01;
						$flag = 0;
						print "d02 not found but d01 found\n";
						last;
				}

				next;				
			}

			if($flag == 1)
			{
				$substr1 = substr ($files_d02[0], length($NCDIR)+1, 23);
				$timestring = substr($substr1, 4);	#used for filename in merge_d02
				print "$substr1 $timestring\n";
				&merge_d02('d02_');
			}

		}
		else
		{
			return;
		}



}

sub merge_d01
{
		my $prefix = $_[0];
		my ($i, $cmd, $untarfile);

		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);

		my $mergefile=$prefix.$timestring.'.nc';
	 	print DBGFILE "$mergefile to be created at $currtime\n";
		my $files = "";
		foreach $i ( 0 .. $#files_d01 )
		{
			chomp($files_d01[$i]);
			`gunzip $files_d01[$i]`;
			my $len = length($NCDIR)+1 + 31;
			$untarfile = substr ($files_d01[$i], 0, $len);

			$files=$files.$untarfile." ";
		}
	
		if($files eq "")
		{
			return;
		}

		$cmd = "rm $FILESDIR/* 2>/dev/null";
		my $retval = system($cmd);		

		$cmd = "mv $files $FILESDIR";
		my $retval = system($cmd);		
		if($retval != 0)
		{
			print DBGFILE "merge_d01: error in mv: $retval $cmd\n";
			return;
		}

		# call concat
		$cmd = "./concatnc $FILESDIR $mergefile";
		$retval = system($cmd);

		if($retval != 0)
		{
			print DBGFILE "error in concat: $retval $cmd\n";
		}

		# move the file to visit run directory
		`mv $mergefile $VISITDIR`;
	 	print DBGFILE "$mergefile moved to $VISITDIR at $currtime\n\n";

		print "\nfiles_d01 =\n@files_d01\n";
		$cmd = "rm @files_d01";
		$retval = system($cmd);		
		if($retval != 0)
		{
			print DBGFILE "error in rm: $retval $cmd\n";
			return;
		}

		$cmd = "rm $FILESDIR/* 2>/dev/null";
		$retval = system($cmd);		
		if($retval != 0)
		{
			print DBGFILE "error in rm: $retval $cmd\n";
			return;
		}

}

sub merge_d02
{
		my $prefix = $_[0];
		my ($i, $cmd, $untarfile);
		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
		my $mergefile=$prefix.$timestring.'.nc';
	 	print DBGFILE "$mergefile to be created at $currtime\n";
		my $files = "";
		foreach $i ( 0 .. $#files_d02 )
		{
			chomp($files_d02[$i]);
			`gunzip $files_d02[$i]`;
			my $len = length($NCDIR)+1 + 31;
			$untarfile = substr ($files_d02[$i], 0, $len);
			$files=$files.$untarfile." ";
		}
		print "files =\n$files\n";
		
		$cmd = "rm $FILESDIR/* 2>/dev/null";
		my $retval = system($cmd);		

		$cmd = "mv $files $FILESDIR";
		my $retval = system($cmd);		
		if($retval != 0)
		{
			print DBGFILE "error in mv: $retval $cmd\n";
			return;
		}

		# call concat
		$cmd = "./concatnc $FILESDIR $mergefile";
		$retval = system($cmd);

		if($retval != 0)
		{
			print DBGFILE "error in concat: $retval $cmd\n";
		}

		# move the file to visit run directory
		`mv $mergefile $VISITDIR`;
	 	print DBGFILE "$mergefile moved to $VISITDIR at $currtime\n\n";

		$cmd = "rm @files_d02";
		$retval = system($cmd);		
		if($retval != 0)
		{
			print DBGFILE "error in rm: $retval $cmd\n";
			return;
		}

		$cmd = "rm $FILESDIR/*";
		$retval = system($cmd);		
		if($retval != 0)
		{
			print DBGFILE "error in rm: $retval $cmd\n";
			return;
		}

}
