# InST: An Integrated Steering Framework for Critical Climate Applications. #

Online remote visualization and steering of critical climate applications like cyclone tracking are essential for effective and timely analysis by geographically distributed climate science community. Unlike existing efforts on computational steering, a steering framework for controlling the high performance simulations of critical climate events needs to take into account both the steering inputs of the scientists and the criticality needs of the application including minimum progress rate of simulations and continuous visualization of significant events. In this work, we have developed an integrated user-driven and automated steering framework for simulations, online remote visualization, and analysis for critical climate applications. Our framework provides the user control over various application parameters including region of interest, resolution of simulations, and frequency of data for visualization.

However, the framework considers the criticality of the application, namely, the minimum progress rate needed for the application, and various resource constraints including storage space, network bandwidth, and number of processors, to decide on the final parameter values for simulations and visualization. Thus our framework tries to find the best possible parameter values based on both the user input and resource constraints. We have demonstrated our framework using a cross-continent steering of a cyclone tracking application involving a maximum of 128 processors. Experimental results show that our framework provides high rate of simulations, continuous visualizations, and also performs reconciliation between algorithm and user-driven computational steering.

* InSt is developed at Indian Institute of Science by personnel of Supercomputer Education and Research Centre (SERC) and Computer Science & Automation (CSA) departments.

* Project description page at MARS lab: http://mars.serc.iisc.ernet.in/software/inst/

### Installation ###

Read **Instructions.odt** for Installation guidelines and using the application

### Contributors ###

* Preeti Malakar (VGL and MARS)
* Akshay Sangodkar (MARS)


### References ###
1. Preeti Malakar, Vijay Natarajan, and Sathish Vadhiyar.
An adaptive framework for simulation and online remote visualization of critical climate applications in resource-constrained environments. 
Supercomputing Conference (SC 2010), New Orleans LA, Nov 2010.

2. Preeti Malakar, Vijay Natarajan, and Sathish Vadhiyar.
INST: An integrated steering framework for critical weather applications. 
ICCS 2011: Proc. International Conference on Computational Science, Procedia Computer Science, 4, 2011, 116-125.