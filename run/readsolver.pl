#!/usr/bin/perl
#this program gets inputs for the optimization problem and populates the solver.mod file

use strict;

my ($filename, $final, $filestring);
my (@file, $cmd, $i, $line, @output, $algonum);
#declare time to solve, Fp/Sp, TFp/Sp
my ($t, $z, $y);

my $optdbgfile='dbgopt';
open (OPTDBG, ">>$optdbgfile") || die "$optdbgfile open failed: $!\n";

my $currtime = `date \+\"\%d\-\%m\-\%y \%T"`; chomp($currtime);

#actual input file to glpsol
$final = 'problem.mod';

$algonum = $ARGV[0];

#input template file
if($algonum == 1)
{
	$filename = 'solver1.mod';
}
else
{
	$filename = 'solver2.mod';
}

#read the template file
open(FILE,  "< $filename ") || die "$filename open failed: $!\n";
@file = <FILE>;
close(FILE);

$filestring = $file[0];
for($i = 1 ; $i <= $#file  ; $i++)
{
        $line = $file[$i];
        $filestring = $filestring.$line;
}

#print OPTDBG "readsolver: LB=$ARGV[0] TIO=$ARGV[2] RHS=$ARGV[3] FACT=$ARGV[4] TLB=$ARGV[5] TSBYRATE=$ARGV[6]\n\n";

#fill in the template
$filestring =~ s/ LB/ $ARGV[1]/g;     #beware TLB is same
$filestring =~ s/UB/$ARGV[2]/;
$filestring =~ s/TIO/$ARGV[3]/g;
$filestring =~ s/RHS/$ARGV[4]/;
$filestring =~ s/FACT/$ARGV[5]/;
$filestring =~ s/TLB/$ARGV[6]/;
$filestring =~ s/TSBYRATE/$ARGV[7]/;

#write the actual .mod file for glpk
open OUT, "> $final";
print OUT $filestring;
close OUT;

#execute glpk and get the output
$cmd = "glpsol -m $final";
@output = `$cmd`;

#read the values of t z y
$t = $output[$#output - 3]; $z = $output[$#output - 2]; $y = $output[$#output - 1];

chomp($t); chomp($z); chomp($y);
print "$t $z $y";	#dont change this, it returns values to optdec.pl
