// Developed by Preeti 16-AUG-2010

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

using namespace std;

FILE *dbg;
int sockfd, visItsockfd, address_len;
socklen_t visIt_address_len;
struct sockaddr_in address, visIt_address;
char buf[512], visbuf[512];
ssize_t readlen;
static const int signum=SIGRTMIN+1; 
struct sigevent sigevent;   

void handler(int signum)
{
	FILE *infile;
	int fwkmsgsize;
//	char *fwkmsg = (char *)malloc(10000 * sizeof(char));	
	char fwkmsg[10000];	

	cout << "Hi I am handler" << endl;

	if((readlen=(recv(visItsockfd, visbuf, sizeof(visbuf), 0)))> 0) {
		buf[readlen]='\0';
		cout << "buf " << visbuf << " readlen " << readlen << endl << endl;
	}

	//sends buf to jobhandler

	//this scanf should be done later at simulation end automated tuning engine...
	//sscanf(buf, %d %d %d %d", &nestResolution, &simResolution, &outputInterval, &simRate);

	char *cmd = (char *)malloc(512*sizeof(char));
	char *optcmd = "./optdec.pl 1512 ";
	strcpy(cmd, optcmd);
	strcat(cmd, visbuf);
	int len = strlen(optcmd) + readlen;
	cmd[len] = '\0';

	system(cmd);
	cout << "cmd " << cmd << " len " << len << " strlen(optcmd) " << strlen(optcmd) << endl;


	sleep(90);	//allow time

	infile = fopen("simd.inf", "r");	
	if(infile) {
		cout << "simd.inf present\n";
		fscanf(infile, "%[^\n]", fwkmsg);
		fwkmsgsize = strlen(fwkmsg);
		cout << "message [" << fwkmsg << "] size " << fwkmsgsize << endl;
		fclose(infile);
		system("rm simd.inf");

		//inform visd
		if(fwkmsgsize > 0)
			write(visItsockfd, fwkmsg, fwkmsgsize);
	}


}

/*
 * Main program
 * 
 * No Arguments
 * 
 * // * Visualization site address 
 *
 */
int main(int argc, char **argv)
{
	// ***************** Variable Declaration *****

/*	if(argc < 2)
		cout << "Missing argument \nUsage: ./simdaemon <visualization-site-address>" << endl;*/

	// Open a socket connection with visualization site
	// Let vis be client and sim be server

	//Server code

	// Create an endpoint for communication

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("simdaemon: socket error ");
		exit(1);
	}

	// Fill socket structure with host information

	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_port = htons(6666);		//6633 if hop
	address.sin_addr.s_addr = INADDR_ANY;

	if (bind(sockfd, (struct sockaddr *) &address, sizeof(address)) == -1) {
		perror("simdaemon: bind error ");
		exit(1);
	}

	if (listen(sockfd, 5) == -1) {
		perror("simdaemon: listen error ");
		exit(1);
	}
	
	visIt_address_len = sizeof(visIt_address);
	if ((visItsockfd = accept(sockfd, (struct sockaddr *) &visIt_address, &visIt_address_len)) == -1) {
		perror("simdaemon: accept error ");
		exit(1);
	}


	cout << "client connected" <<endl;

	signal(SIGIO, handler);

	if (fcntl(visItsockfd, F_SETOWN, getpid()) == -1) {
	    perror("F_SETOWN");
	    exit(1);
	}
	if (fcntl(visItsockfd, F_SETFL, fcntl(visItsockfd, F_GETFL, 0) | FASYNC) == -1) {
	    perror("F_SETFL");
	    exit(1);
	}


 	while(1)
	{
		sleep(20);
	}


	//never reaches here ...


	// Close connection
	close(sockfd);
	close(visItsockfd);

	return 0;

}
