#!/usr/bin/perl
#This is the main driver for the optres
#this program gets input for the optimization problem and populates the solver.mod file
#
#input to this program is resolution in km
#this program writes a file called change when it thinks that the parameters should be changed
#if changes in parameters are there then it creates change and copies to oldparams ow it copies oldparams to change ie oldparams and change are same wen this pgm exits

#invoked periodically by calloptdec.c, or on some special event

use strict;
use POSIX qw(ceil);
use POSIX qw(floor);
use Switch;

#Variable Declarations

#File names
our $debugfile = 'debug-jobsubmit';
our $input = 'change';
our $oldparams = 'oldparams';
our $CONFIGFILE = 'optdec.config';
our $SETUPFILE = 'setup.config';
our $remperfile='remper';
our $optdbgfile='dbgopt';
our $steeringdbg='steeringdbg';
our $changeStartDateFile = 'changeStartDate';
our $nestPositionFile = 'nestposition';

#low disk threshold percentage
our $LOWDISK;

#Maximum available memory in hard disk in GB  
our $MAXDISK;

#Initial disk space WRF requires
our $INITDISK = 1.5;

#Minimum and maximum history interval (HI)/ Interval to output WRF output files  
our ($MINHI, $MAXHI);

#Default maximum history interval (HI)
our $FIXEDMAXHI = 30;

#When optdec.pl is called periodically, calloptdec.c passes 3721 as $ARGV[0] to signal no change in resolution. When optdec.pl is called by simdaemon the value 1512 as ARGV[0] is passed.
our $new_res = $ARGV[0];		

#Frames per output defaulted to 1
our $fpo = 1;

#Input arguments to optdec.pl
# $formNest - Boolean to indicate nest formation
# $nestXPos - i_parent_start of nest domain 
# $nestYPos - j_parent_start of nest domain
# $nestRes  - Nest domain resolution
# $simRes   - Parent domain resolution
# $OI       - WRF output history interval
# $Rate     - Simulation Minimum Progress Rate (MPR)
# $startDate- New simulation start date
our ($formNest, $nestXPos, $nestYPos, $nestRes, $simRes, $OI, $Rate, $startDate);

#Output size per timestep (1p + 3n) for different resolutions
our %output_size = ();

#Time to output one timestep (1p + 3n)
our %tio = ();

#the least time for each resolution
our %tts = ();

#Simulation progress rate
our ($simrate, $maxsimrate, $defaultmaxsimrate) = (5.0, 5.0, 5.0);

#For the glpk
our ($D, $O, $n, $b, $res, $tpbyrate);
our ($t, $z, $y) ;

#choose algorithm 1 or 2
our $algo;

#input to the optimizer
our ($LB, $UB, $TIO, $RHS, $FACT, $tp, $TLB);

#Other variables
my ($oldtime, $newtime, $MINtime, $MAXtime);
my ($cmd, $i, $history_interval, $temp, $ret, $output, $var, @killcmd);
our ($usedper, $avail, $remper, $oldHI, $newHI);
our ($oldnumprocs, $newnumprocs, @oldparameters);
our $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
our $regexp = qr/[a-zA-Z](?:[a-zA-Z0-9_])*/; # allowed var names
our @config;
my ($line,$i, $groupflag);
our $changeStartDateFlag;

chomp($currtime);


#Read tio and tts from setup.config
print "Reading file $SETUPFILE\n";
open(STFILE, $SETUPFILE) || die "Could not read from $SETUPFILE";

while(<STFILE>) {

    if($_ =~ m/^network_bandwidth*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	$b = $_;
    }
    if($_ =~ m/^tio*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	%tio = split(' ',$_);
    }
    if($_ =~ m/^tts*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	%tts = split(' ',$_);
    }
    if($_ =~ m/^output_size*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	%output_size = split(' ',$_);
    }
}
close STFILE;


#Read the required parameters from optdec.config and populate  the parameters $LOWDISK, $MAXDISK, $MINHI and $MAXHI

print "Reading file $CONFIGFILE\n";
open(FILE, $CONFIGFILE) || die "Could not read from $CONFIGFILE";
@config = <FILE>;
close FILE;

$groupflag = 0;

for($i = 0 ; $i < scalar(@config) ; $i++) {
    
    $line = $config[$i];
    chomp($line);

    # Strip leading space and anything from possible leading exclamation mark till the end of line.
    $line =~ s/^(\s*(![^\n]*)?)*//s;

    #skip a line which has only a carriage return
    if ($line =~ m/^\n/) {
	next;
    }

    #Check if the parameter group optdec.pl exists, if yes then set flag = 1
    if ($line =~ m/^&optdec.pl/) {
	$groupflag = 1;
	print "$line\n";
	next;
    }

    #read all the parameters in the parameter group 'optdec.pl'
    if ($groupflag == 1){
	if($line =~ m/^LOWDISK*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $LOWDISK = $line;
	    print "LOWDISK = $LOWDISK\n";
	}
	elsif($line =~ m/^MAXDISK*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $MAXDISK = $line;
	    print "MAXDISK = $MAXDISK\n";
	}
	elsif($line =~ m/^MINHI*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $MINHI = $line;
	    print "MINHI = $MINHI\n";
	}
	elsif($line =~ m/^MAXHI*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $MAXHI = $line;
	    print "MAXHI = $MAXHI\n";
	}
        elsif($line =~ m/^\//){
	    $groupflag = 0;
	    last;
	}
    }
}


open (REMPER, ">>$remperfile") || die "$remperfile open failed: $!\n";
open (OPTDBG, ">>$optdbgfile") || die "$optdbgfile open failed: $!\n";
open (STRDBG, ">>$steeringdbg") || die "steeringdbg open failed: $!\n";


#   *****            START           *****

print OPTDBG "\n$currtime OPTDEC.PL: I am starting\n";
`echo "OPTDEC.PL: I am starting at $currtime" >> $debugfile`;

#read oldparams file
open(IFH,  "< $oldparams ") || die "$oldparams open failed: $!\n";
@oldparameters = <IFH>;
close(IFH);

print OPTDBG "\n$currtime parameters read:\n@oldparameters\n";

$oldHI=$oldparameters[0];
chomp($oldHI);
$oldnumprocs=$oldparameters[1];
chomp($oldnumprocs);
$fpo=$oldparameters[2];
chomp($fpo);

#assume no change
$newHI = $oldHI;
$newnumprocs = $oldnumprocs;



#1512 means the optimal decision algorithm will use steering. 7 arguments are expected.
if($ARGV[0] == 1512)
{

#check if there are 8 arguments passed namely resolution/code, isformNest, nestXPos, nestYPos, nestRes, simRes, Output Interval (OI), Progress Rate, startDate. For example, $./optdec.pl 1512 0 -1 -1 -1 -1 23 10 2009-05-25_18:00:00
	if($#ARGV != 8)
	{
		print "$#ARGV\n";
		exit;
	}

#initialize variables
	($formNest, $nestXPos, $nestYPos, $nestRes, $simRes, $OI, $Rate, $startDate) = ($ARGV[1], $ARGV[2], $ARGV[3], $ARGV[4], $ARGV[5], $ARGV[6], $ARGV[7], $ARGV[8]);
	print STRDBG "$currtime: $ARGV[0]  $formNest, $nestXPos, $nestYPos, $nestRes, $simRes, $OI, $Rate, $startDate\n";


	if($formNest == 1)
	{
		if(($nestXPos == -1) || ($nestYPos == -1))
		{
		        open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
			print INFORM "$currtime: Please specify X and Y positions to form nest.\n";
			close(INFORM);
			print STRDBG "$currtime: Please specify X and Y positions to form nest.\n";
			exit;
		}

		my $formnestfile='formNest';
		open (FMN, ">$formnestfile");
		print FMN "1\n$nestXPos\n$nestYPos\n0\n";
		close (FMN);

		open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
		print INFORM "Done!";
		print STRDBG "$currtime: Dear User: Message from the steering framework:\nformNest Done! SimRes/OI/Rate/startDate if specified will be ignored.\n";
		close(INFORM);
		exit;
	}


	if($startDate != -1)
	{
                #Match startDate string of type 2009-05-22_18:00:00
		if(!($startDate =~ m/^\d\d\d\d-\d\d-\d\d_\d\d:\d\d:\d\d/))
		{
			open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
			print INFORM "$currtime: Invalid \$startDate: $startDate\n Acceptable startDate is of the format yyyy-mm-dd_HH:MM:SS\n";
			close(INFORM);
		        print STRDBG "$currtime: Invalid \$startDate: $startDate\n Acceptable startDate is of the format yyyy-mm-dd_HH:MM:SS\n";
			exit;
		}

		#X and Y coordinates of the nest position is mandatory when changing the start date to the past. Hence it is either provided by the user 
		if(($nestXPos == -1) || ($nestYPos == -1))
		{
		        my (@npfile, $i, $line, @tokens, $substr, $substr1, $factor, $ret, $chres);

			#Open nestposition file and find the X and Y coordinates for $startDate
		        open(NPF,  "< $nestPositionFile") || die "$nestPositionFile open failed: $!\n";
			@npfile = <NPF>;
			close(NPF);

			for($i = 0; $i < $#npfile ; $i++)
			{
			        $line = $npfile[$i];
				chomp($line);
				@tokens = split(/ /, $line);
                                #Example: token[0] = 2009-05-23_12:00:09
				$substr = substr $tokens[0], 0, 14;

				$substr1 = substr $startDate, 0, 14;
				if ($substr1 =~ /$substr/)
				{
				        print STRDBG "\ncurrtime: startDate  = $startDate matched with nestposition = $line\n";
					#Adjust X and Y nest positions if the resolution at which the nest position was stored was different
					if($simRes != -1)
					{
					        #this is converting it to the new scale since now the resolution might have changed
					        $factor = $tokens[3]/$simRes;        #old resolution/new resolution
						
					}
					else
					{
					        #Get current resolution
					        $cmd="grep dx namelist.input | awk \-F' ' '{print \$3}'"; 
						$ret = `$cmd`;
						chomp($ret);
						$chres = substr $ret, 0, length($ret) - 1;	#remove the comma at the end of $ret
						$factor = $tokens[3]/$chres;        #old resolution/new resolution
					}

					$nestXPos = floor($tokens[1] * $factor);
					$nestYPos = floor($tokens[2] * $factor);
					print STRDBG "\ncurrtime: nestXPos = $nestXPos; nestYPos = $nestYPos\n";
					last;
				}
			}
			
			#If nest position is still not found then throw an error
			if($nestXPos == -1)
			{
			        open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
				print INFORM "$currtime: The past date is beyond the time when the nest was formed. Please specify a date after the nest was formed.\n";
				close(INFORM);
				print STRDBG "$currtime: The past date is beyond the time when the nest was formed. Please specify a date after the nest was formed.\n";
				exit;
			}
		}

	        #If OI and MPR is not specified by user then create changeStartDateFile here. Else do the steering checks and create changeStartDate file at the end.
		if(($OI == -1) && ($Rate == -1)) 
		{
		        open(CSD, ">$changeStartDateFile");
			print CSD "$startDate\n$nestXPos\n$nestYPos\n$simRes\n$OI\n-1";
			close(CSD);

			open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
			print INFORM "Done!";
			print STRDBG "$currtime: Dear User: Message from the steering framework:\nchange Start Date Done!\n";
			close(INFORM);
			exit;
		}
	        else
		{
		        #Specification of simulation resolution and OI/Rate cannot be clubbed together.
		        if($simRes != -1)
			{
			        open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
				print INFORM "$currtime: Please specify simRes and OI/Rate as separate events.\n";
				close(INFORM);
				print STRDBG "$currtime: Please specify simRes and OI/Rate as separate events.\n";
				exit;
			}
			elsif(($OI != -1) && ($Rate != -1))
			{
			        open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
				print INFORM "$currtime: Please specify OI and Rate as separate events.\n";
				close(INFORM);
				print STRDBG "$currtime: Please specify OI and Rate as separate events.\n";
				exit;
			}
		        $changeStartDateFlag = 1;
		}
	}

	
	if($simRes != -1 and $OI == -1 and $Rate == -1)
	{
		my %pres_res = ('21000', '99500', '18000', '99100', '15000', '98900', '12000', '98700');
		my $value = $pres_res{$simRes};
		my $nestfile='finerNest';
		open (FMN, ">$nestfile");
		print FMN "$value\n1\n1\n";
		close (FMN);

		open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
		print INFORM "Done!";
		print STRDBG "$currtime: Dear User: Message from the steering framework:\nfinerNest  Done!\n";
		close(INFORM);
		exit;
	}

	if($OI != -1 and $Rate != -1)
	{
	        open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
		print INFORM "$currtime: Please specify OI and Rate as separate events.\n";
		close(INFORM);
		print STRDBG "$currtime: Please specify OI and Rate as separate events.\n";
		exit;
	}

	if($simRes != -1)
	{
		$new_res = $simRes;
	}
	else
	{
		$new_res = 3721;
	}
}







#SECTION: Check if this is invoked periodically or invoked by the user without resolution specification. Assign $new_res and $simRes with the parent resolution in namelist.input
##############################################
if($new_res == 3721)
{
        #find out current resolution
	$cmd="grep dx namelist.input | awk \-F' ' '{print \$3}'"; 
        $ret = `$cmd`;
        chomp($ret);
        $new_res = substr $ret, 0, length($ret) - 1;	#remove the comma at the end of $ret
	$simRes = $new_res;

        print OPTDBG "\n$currtime optdec: resolution ret=$ret\n";
        print STRDBG "\n$currtime optdec: resolution ret=$ret\n";
        print OPTDBG "\n$currtime optdec: No parameter passed, so being called periodically, so setting new_res to current value $new_res\n";
}
#SECTION: END



#SECTION: CHECK IF DISK SPACE IS TOO LOW
#################################
print OPTDBG "\noptdec: $currtime new_res=$new_res\n";

#find currently occupied disk space
$cmd = "du -sh . | awk -F' ' '{print \$1}' 2> /dev/null";
$output = `$cmd`;
chomp($output);
my $used = substr($output, 0, length($output) - 1);
#Check if the disk space is in MB or KB
if($output =~ m/M/)
{
	$used = $used/1024.0;
}
elsif($output =~ m/K/)
{
	$used = $used/(1024.0 * 1024.0);
}

print OPTDBG "du -sh output [$output] used $used\n";
my $avail = $MAXDISK - $used;
$remper = $avail * 100.0 / ($MAXDISK - $INITDISK);	# minus the initial space WRF occupies

print OPTDBG "\n$currtime OPTDEC.PL: Avail Disk Space = $avail, Rem disk % = $remper, % MAXDISK = $MAXDISK GB\n\n";
print REMPER "OPTDEC.PL: Time= $currtime Used= $used Available= $avail Remaining%= $remper\n";


#Check if free disk space is below LOWDISK threshold percentage
if ($remper < $LOWDISK)
{
#stall
	`touch stall`;  #to be checked by jobsubmit and should wait in a loop and check for wakeup

	print OPTDBG "\n$currtime OPTDEC.pl; remper=$remper < LOWDISK $LOWDISK, so killing wrf\n";

#Kill WRF
	@killcmd = `head \-1 rsl.error.* | grep taskid  |awk \'{print \$NF}\'  |uniq`;
	print OPTDBG "\n$currtime optdec.pl hostnames:\n@killcmd\n";
	for $i (0 .. $#killcmd-1)
	{
		chomp($killcmd[$i]);
		print OPTDBG "\n$currtime optdec.pl kill wrf in hostname:$killcmd[$i]\n";
		`ssh $killcmd[$i] ./killscript`;
	}

#Wait till remaining disk space is above 15%
	while ($remper < 15)
	{
		sleep(120);

		$cmd = "du -sh . 2> /dev/null";
		$output = `$cmd`;
		chomp($output);
		my $used = substr($output, 0, length($output) - 1);
		my $avail = $MAXDISK - $used;
		$remper = $avail * 100.0 / ($MAXDISK - $INITDISK);	

		print OPTDBG "\n$currtime optdec.pl: STALLING: Avail Disk Space = $avail, Rem disk % = $remper\n";
		print REMPER "\n$currtime optdec.pl: STALLING: Avail Disk Space = $avail, Rem disk % = $remper\n";
	}

	`rm stall`;
	`touch wakeup`;		#wakeup is checked by jobsubmit.pl
}

#SECTION: END



#Initializing 
$n = 5.0 * 3600.0;              #hours default	#calculated later in calculate()
$D = $avail;			#in GB
$algo = 1;			#assume there is disk constraint, calculate will find out if there isnt any



#SECTION: Steering 
##########################################
if(($ARGV[0]==1512) and (($Rate != -1) or ($OI != -1)))
{	
	my $first_res = $new_res;
	while(1)
	{
		$first_res = $new_res;

		&steering();
		
		if($first_res == $new_res)
		{
			last;
		}
		#else we are just considering options
		$tp = $new_res * 0.003; 
	}
}
#SECTION: END


#SECTION: Default optimization (periodically invoked)
#############################################
else
{
	print OPTDBG "$ARGV[0] new_res $new_res\n";
#calculate the variable parameters which are based on resolution
	&calculate();

#calculate maximum feasible rate considering Maximum HI possible ($MAXHI) and minimum time to solve a time step $TLB
	$maxsimrate = $tp/($TLB + $TIO*$tp/($MAXHI*60.0));

	print OPTDBG "\nmaxsimrate possible = $maxsimrate\n";

	if($simrate > $maxsimrate)
	{
		print OPTDBG "\n$currtime: simrate > maxsimrate: $simrate > $maxsimrate\n";
		if($maxsimrate > $defaultmaxsimrate)
		{	
			$simrate = $defaultmaxsimrate;
		}
		else
		{		
			$simrate = $maxsimrate;
		}
	}
}
#SECTION: END


#SECTION: Solution using readsolver.pl. Calculate HI and numprocs
#################################################
$tpbyrate=$tp*1.0/$simrate;
print OPTDBG "\n$currtime: tp=$tp simrate=$simrate\n";
print OPTDBG "\n$currtime parameters to readsolver: ALGO=$algo LB=$LB UB=$UB TIO=$TIO FACT=$FACT RHS=$RHS TLB=$TLB tp/RATE=$tpbyrate\n";

$cmd = `./readsolver.pl $algo $LB $UB $TIO $RHS $FACT $TLB $tpbyrate`;
($t, $z, $y) = split(/ /,$cmd);

print OPTDBG "$currtime optdec.pl: t=$t, z=$z, y=$y\n";

if( $z != 0)
{
	$newHI = ceil($tp/($z*60.0));   #in min #Hp = tp / z
		if ($newHI < $MINHI)
		{
			print OPTDBG "\nSetting newHI=$newHI to $MINHI\n";
                $newHI = $MINHI;
                print OPTDBG "\nSet newHI=$newHI\n";
        }
}
else
{
        print OPTDBG "\n$currtime optdec: z = 0 RETURN\n";
        exit;
}

print OPTDBG "$currtime optdec.pl: t=$t, z=$z, y=$y newHI=$newHI\n";
print OPTDBG "$currtime optdec: R = tp/(t+TIO.z) = ".$tp*1.0/($t+$TIO*$z)."\n";

$newtime = $t;
$newnumprocs = &get_procs($new_res, $newtime);

print OPTDBG "\n$currtime optdec newHI = $newHI, oldHI = $oldHI\n";
print OPTDBG "\n$currtime optdec newnumprocs = $newnumprocs, oldnumprocs = $oldnumprocs\n";

if($newHI < $MINHI)
{
	$newHI = $MINHI;
}
elsif($newHI > $MAXHI)
{
	$newHI = $MAXHI;
}

if($newHI%3 != 0)
{
	$newHI= ($newHI%3 == 1)? $newHI+2 : $newHI+1;
	print OPTDBG "$currtime optdec changed to multiple of 3\n";
}

print OPTDBG "\n$currtime optdec newHI = $newHI, oldHI = $oldHI\n";
print OPTDBG "\n$currtime optdec newnumprocs = $newnumprocs, oldnumprocs = $oldnumprocs\n";
#SECTION: END



#now write to a file if anything is changed
if(($oldHI ne $newHI) or ($oldnumprocs ne $newnumprocs))
{
	#Checks whether the start date is being changed and uses changeStartDateFile instead of change file
	if($changeStartDateFlag == 1)
	{
	        print OPTDBG "\n$currtime optdec.PL: Parameters changed: $newHI $oldHI, $newnumprocs $oldnumprocs : Hence writing to changeStartDateFile\n"; 
	        $changeStartDateFlag = 0;
		open(CSD, ">$changeStartDateFile");
		print CSD "$startDate\n$nestXPos\n$nestYPos\n-1\n$newHI\n$newnumprocs";
		close(CSD);

	}
	else
	{
	        print OPTDBG "\n$currtime optdec.PL: Parameters changed: $newHI $oldHI, $newnumprocs $oldnumprocs : Hence writing to change\n"; 
                #write change file
	        open (FH, ">$input") || die "optdec: $input write mode open failed: $!\n";
		print FH "$newHI\n";
		print FH "$newnumprocs\n";
		print FH "$fpo\n";
		close(FH);
		system ("cp $input oldparams");
        }
}

else
{
#oldparams remain unchanged
        print OPTDBG "\n$currtime:optdec.PL: Parameters not changed: $newHI $oldHI, $newnumprocs $oldnumprocs\n";
}

`echo "optdec.PL: I am exiting at \`date\`" >> $debugfile`;

close(STRDBG);
close(OPTDBG);
close(REMPER);
close(FH);

exit;



sub steering()
{
#get non-steering values first
	&calculate();		#get TLB TIO
	$maxsimrate = $tp/($TLB + $TIO*$tp/($MAXHI*60.0));	#calculate maximum feasible rate considering FIXEDMAXHI

	print STRDBG "\n$currtime before cal: ALGO=$algo LB=$LB UB=$UB TIO=$TIO FACT=$FACT RHS=$RHS TLB=$TLB\n";
	print STRDBG "\n$currtime: maxsimrate $maxsimrate new_res $new_res\n";

	if( ($Rate != -1) and ($OI != -1))		#ie both rate and OI mentioned
	{
		print STRDBG "1\n";
		$simrate = 1.0*$Rate;
		$MAXHI = $OI;
		&calculate();
		$maxsimrate = $tp/($TLB + $TIO*$tp/($MAXHI*60.0));	#ie the max rate possible with the specified by user

		print STRDBG "\nRate != -1 and OI != -1 maxsimrate $maxsimrate simrate $simrate\n";

		my $usrmsg;
		if($simrate > $maxsimrate)
		{
			print STRDBG "2\n";

			#append to string
			$usrmsg = "The given OI $OI is not possible with given rate $Rate\n";#Hence system overriding OI with $FIXEDMAXHI\n";
			$MAXHI = $FIXEDMAXHI;
			&calculate();
			$maxsimrate = $tp/($TLB + $TIO*$tp/($MAXHI*60.0));

			#check again if rate and OI are compatible
			if($simrate > $maxsimrate)		#if not
			{
				print STRDBG "3\n";
				my $maxrateforuser = floor($maxsimrate);
				if($new_res == 24000)
				{
					my $nextmsg = "Given Rate $Rate is also not possible even with maximum resolution of $new_res. ";
					$usrmsg = $usrmsg.$nextmsg;
					open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
					print INFORM $usrmsg;
					print STRDBG "$currtime: $usrmsg \n";
					close(INFORM);
					exit;
				}
				else
				{
					$new_res = $new_res + 3000;		#check for higher resolution
					return;
				}
			}
			else
			{
				print STRDBG "4\n";
				if($new_res == $simRes)
				{
					my $nextmsg = "Hence system overriding OI with $FIXEDMAXHI. ";
					$usrmsg = $usrmsg.$nextmsg;
					open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
					print INFORM $usrmsg;
					print STRDBG "$currtime: $usrmsg ";
					close(INFORM);
				}
			}
		}
		
		if($new_res != $simRes)
		{
			print STRDBG "5\n";
			my $nextmsg = "But given Rate $Rate is possible with resolution of $new_res. ";
			$usrmsg = $usrmsg.$nextmsg;

			open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
			print INFORM $usrmsg;
			print STRDBG "$currtime: $usrmsg \n";
			close(INFORM);
			exit;
		}
	}
	elsif(($Rate != -1) and ($OI == -1))		
	{
		print STRDBG "6\n";
		$simrate = 1.0*$Rate;
		$MAXHI = $FIXEDMAXHI;
		&calculate();
		$maxsimrate = $tp/($TLB + $TIO*$tp/($MAXHI*60.0));
		my $minsimrate = $defaultmaxsimrate > $maxsimrate ? $maxsimrate : $defaultmaxsimrate;
		if($simrate < int($minsimrate))
		{
			print STRDBG "7\n";
			my $foruser = int($minsimrate);
			open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
			print INFORM "Please specify a minimum rate of $foruser. ";
			print STRDBG "$currtime: Dear User: Message from the steering framework:\nPlease specify a minimum rate of $foruser\n";
			close(INFORM);
			exit;
		}

		if($simrate > $maxsimrate)
		{
		        print STRDBG "8\n";
			my $maxrateforuser = floor($maxsimrate);

			print STRDBG "\nResolution  MaxMPR";
			foreach $new_res (keys %tts) {
			        &calculate();
				$maxsimrate = $tp/($TLB + $TIO*$tp/($MAXHI*60.0));
				print STRDBG "\n$new_res $maxsimrate";
			}
			print STRDBG "\n";
			open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
			print INFORM "Given Rate $Rate is not possible, maximum possible rate is $maxrateforuser. ";
			print STRDBG "$currtime: Dear User: Message from the steering framework:\nGiven Rate $Rate is not possible, Maximum possible rate is $maxrateforuser\n";
			close(INFORM);
			exit;
		}

	}
	elsif(($Rate == -1) and ($OI != -1))	
	{
		print STRDBG "9\n";
		$MAXHI = $OI;
		&calculate();
		$maxsimrate = $tp/($TLB + $TIO*$tp/($MAXHI*60.0));

		#Calculate the maximum OI ($foruser2) which can sustain minimum MPR of 5 (defaultmaxsimrate)
		my $foruser2 = ceil(($TIO * $tp)/(60.0 * (($tp/$defaultmaxsimrate) - $TLB)));
		print STRDBG "\n foruser2 = $foruser2, uOI = $MAXHI,  maxMPR = $maxsimrate";

		if($maxsimrate < $defaultmaxsimrate)
		{
		        my ($foruser,$foruser1);
		        print STRDBG "10\n";
			$foruser = ceil(($TIO * $tp)/(60.0 * (($tp/$defaultmaxsimrate) - $TLB)));
			$foruser= ($foruser%3 == 1)? $foruser+2 : $foruser+1;
			
			#Calculate the maximum OI (foruser1) for each resolution which can sustain minimum MPR of 5 (defaultmaxsimrate)
			foreach $new_res (keys %tts) {
			        &calculate();
				$foruser1 = ceil(($TIO * $tp)/(60.0 * (($tp/$defaultmaxsimrate) - $TLB)));
				$foruser1 = ($foruser1%3 == 1)? $foruser1+2 : $foruser1+1;
				print STRDBG "\n$new_res $foruser1"; 
			}
			print STRDBG "\n";

			open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
			print INFORM "Given OI $OI is not possible, min OI required is $foruser. ";
			print STRDBG "$currtime: Dear User: Message from the steering framework:\nGiven OI $OI is not possible, min OI required is $foruser\n";
			close(INFORM);
			exit;
		}
		$simrate = $maxsimrate;		#let rate be max correspoding to OI
	}

	print STRDBG "$currtime: After if loop: rate $Rate OI $OI simrate $simrate maxsimrate $maxsimrate\n";
	print STRDBG "\n$currtime after cal: ALGO=$algo LB=$LB UB=$UB TIO=$TIO FACT=$FACT RHS=$RHS TLB=$TLB simrate $simrate\n";

	open (INFORM, "> simd.inf") || die "simd.inf open failed: $!\n";
	print INFORM "Done!";
	print STRDBG "$currtime: Dear User: Message from the steering framework:\nDone!\n";
	close(INFORM);
	return;
}


# Helper functions start

sub calculate()
{

#       lookup tp, Op, On for this res
#
#tp : The amount of time simulated per time step. It is constant for a resolution.
        $tp = $new_res * 0.003;         #3 times the resolution in seconds

#O : Size of the netCDF output for a simulation timestep
        $O = $output_size{$new_res};            #in MB

#LB : LowerBound = tp/OI = F/S where OI = MAXHI
        $LB = $tp*1.0/($MAXHI*60.0);   	#converting to sec

#UB : UpperBound = tp/OI = F/S where OI = MINHI
        $UB = $tp*1.0/($MINHI*60.0);

#TIO : Minimum time to output one time step for a particular resolution
        $TIO = $tio{$new_res};

#RHS : O/b 
        $RHS = $O * 1.0 / $b;   #MB / MBps

	print OPTDBG "tp=$tp new_res=$new_res O=$O b=$b TIO=$TIO D=$D UB=$UB LB=$LB\n";

#n : Time for the disk to be full
#n <= D/(Rin - Rout) = D/(O/TIO - b) 
        $n = ceil(($D * 1024.0)/(1.0*$O/$TIO - 1.0*$b));    #this is in seconds 

	if($n < 0)
	{
		#Means R_in < R_out
		#choose algo without disk constraint
		$algo=2;
		$FACT = 0;	#not used in the optimizer
		print OPTDBG "\n $n < 0, so ALGO=2\n";
	}

#FACT
	else
	{
		$algo = 1;	#disk constraint is there assumedly since n > 0

#var : Intermediate variable 
# var = D/n + b
		$var = ($D*1024.0)/($n*1.0) + $b;               #properly converting GB to MB, hr to sec, Mb to MB

#FACT : FACT = O/var - TIO
		$FACT = 1.0*$O/$var - $TIO*1.0;
		print OPTDBG "\n$currtime optdec: ALGO=1, D=$D, n=$n, b=$b. O=$O, var=$var, TIO=$TIO FACT=$FACT RHS=$RHS\n";
	}

#TLB : Minimum time to solve one time step for a particular resolution
        $TLB = $tts{$new_res};
        print OPTDBG "$currtime optdec: tp=$tp D=$D var=$var FACT=$FACT RHS=$RHS TLB=$TLB\n";


#check whether disk constraint is valid
#check for O/b > TIO

	if($O*1.0/$b < $TIO*1.0)
	{
		#Means R_in < R_out
		#choose algo without disk constraint
		$algo=2;
		print OPTDBG "$currtime optdec: No disk constraint: $O*1.0/$b < $TIO\n";
	}
}



#parameters passed are newres newtime
sub get_procs()
{
	my $new_res = $_[0];
	my $adjnewtime = $_[1];

	$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
	chomp($currtime);
	print OPTDBG "\n$currtime optdec.PL:get_procs: newres=$new_res adjnewtime=$adjnewtime newtime=$newtime\n";      #newtime is returned by glpk

	my %solve_times = ();

#Reads solve_times from setup.config
	my $stflag;

	open(STFILE, $SETUPFILE) || die "Could not read from $SETUPFILE";

	while(<STFILE>) {

	    if($_ =~ m/^&solve_times*/) {
		$stflag = 1;
		next;
	    }

	    if ($stflag == 1){
		if($_ =~ m/^($new_res)=*/){
		    chomp;
		    # retain only the value  of the parameter
		    $_ =~ s/^($regexp)\s*=\s*//s ;	    
		    %solve_times = split(' ',$_);
		    next;
		}
		elsif($_ =~ m/^\//){
		    $stflag = 0;
		    last;
		}
	    }
	}

	close STFILE;
	print OPTDBG "\n$currtime optdec.PL:get_procs: newres=$new_res, newtime=$newtime\n";

#Find the highest num procs in solved_times
	my $highest_key = (sort {$b <=> $a} keys %solve_times)[0];
	my $diff = abs($solve_times{$highest_key} - $newtime);
	my $proc = 48;
	my ($key, $value);

	foreach $key (keys %solve_times) {
		$value = $solve_times{$key};

		if (abs($value-$newtime) < $diff)
		{
			print OPTDBG "\noptdec. debug purpose : get_proc: abs = abs($value-$newtime) diff=$diff\n";
			$diff = abs($value-$newtime);
			$proc = $key;
		}
		print OPTDBG "\noptdec. debug purpose : get_proc: $proc $key $value\n";
	}

	return $proc; #return corresponding processor
}

# Helper functions end


