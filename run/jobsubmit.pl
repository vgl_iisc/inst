#!/usr/bin/perl
#this program runs wps, wrf etc, check config files etc

use strict;

use POSIX qw(floor);
use Cwd;
use Switch;

#Variable declarations
our $NODEFILE;
our $NPROCS;
our $OLD_RES = 3721;    #arbitrary
our $MAXPROCS;          #read from oldparams intially and store for the rest of the run
our $dcm = "./optdec.pl";
our $dcmcaller = "./calloptdec";
our $nestpostracker = "./nestpostracker.pl";
our ($old_res, $new_res);
our ($cmd, $numrun, $result, $retval, $tomatch);
our $resumetransfer = 'resumetransfer';
our $timesfile = 'times.log';
our $input = 'namelist.input';
our $wps = '../../WPS/namelist.wps';
our @inputtext = ();
our @wpstext = ();
our %namelist = ();
our %namelistwps = ();
our @press_pa = ();
our $regexp = qr/[a-zA-Z](?:[a-zA-Z0-9_])*/; # allowed variable names in namelist.input 
our ($end_day, $end_hour); 
our ($year, $month, $day, $hour, $minute, $second, $timestamp, $i_parent_start, $j_parent_start, $run_days, $run_hours);
our ($parent_dx, $nest_dx, $parent_dy, $nest_dy);
our ($parent_e_we, $nest_e_we, $parent_e_sn, $nest_e_sn);
our ($nest_start_i, $nest_start_j);
our @changeinput;
our $oldparams='oldparams';
our ($newHI, $newnumprocs, $newfpo);
our ($dom, $currtime);
our $CONFIGFILE = 'optdec.config';
our @config;
our ($MAXDISK, $avail, $output);
our $tracefile = 'tracefile';
our $jobdbgfile = 'jobdbgfile';
our $changeStartDateFile = 'changeStartDate';
our @changeStartDate;
our ($startDate, $nestXPos, $nestYPos, $simRes, $OI, $changeStartDate_flag, $chprocs);

my ($line,$i, $groupflag);
my ($event_flag, $first_start);


#Create executable 'transfer' which periodically calls transfer.pl
`gcc \-o transfer transfer.c`;
#Create executable 'calloptdec' which periodically calls optdec.pl
`gcc \-o calloptdec calloptdec.c`;


####################START DAEMONS#################################
#Start the DCM
$cmd = "$dcmcaller >> tracefile  &";
system($cmd);

#Start nest position tracker
$cmd = "$nestpostracker &";
system($cmd);
#################################################################


open(TRH,  ">> $tracefile ") || die "$tracefile open failed: $!\n";
open(JDBG,  ">> $jobdbgfile ") || die "$jobdbgfile open failed: $!\n";
open(IFH,  "> $oldparams ") || die "$oldparams creation failed: $!\n";


##################Read/Write configuration files #################

#Read the fields end_day, end_month, end_year, end_hour, end_minute, end_second from namelist.input
open(FILE, $input) || die "Could not read from $input";

while(<FILE>) {
    	#Match the field end_day 
	if( m/^\s*(end_day)\s*=\s*(\d+)\s*\,/) {

	    $end_day = $2;
	}
    	#Match the field end_hour 
	elsif( m/^\s*(end_hour)\s*=\s*(\d+)\s*\,/) {

	    $end_hour = $2;
	}
}
close FILE;

print "Reading file $CONFIGFILE\n";
open(FILE, $CONFIGFILE) || die "Could not read from $CONFIGFILE";
@config = <FILE>;
close FILE;


#Reads the required parameters from optdec.config, creates a fresh oldparams file and populates the parameters $newHI, $newnumprocs, $newfpo and $MAXPROCS
$groupflag = 0;

for($i = 0 ; $i < scalar(@config) ; $i++) {
    
    $line = $config[$i];
    chomp($line);

    # Strip leading space and anything from possible leading exclamation mark till the end of line.
    $line =~ s/^(\s*(![^\n]*)?)*//s;

    #skip a line which has only a carriage return
    if ($line =~ m/^\n/) {
	next;
    }

    #Check if the parameter group oldparams exists, if yes then set groupflag = 1
    if ($line =~ m/^&oldparams/) {
	$groupflag = 1;
	print "$line\n";
	next;
    }
    #Check if the parameter group jobsubmit.pl exists, if yes then set groupflag = 2
    if ($line =~ m/^&jobsubmit.pl/) {
	$groupflag = 2;
	print "$line\n";
	next;
    }
    #Check if the parameter group optdec.pl exists, if yes then set groupflag = 3
    if ($line =~ m/^&optdec.pl/) {
	$groupflag = 3;
	print "$line\n";
	next;
    }


    #read all the parameters in the parameter group 'oldparams'
    if ($groupflag == 1){
	if($line =~ m/^HI*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $newHI = $line;
	    print "HI = $newHI\n";
	    print IFH "$newHI\n";
	}
	elsif($line =~ m/^numprocs*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $newnumprocs = $line;
	    print "numprocs = $newnumprocs\n";
	    print IFH "$newnumprocs\n";
	}
	elsif($line =~ m/^FPO*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $newfpo = $line;
	    print "FPO = $newfpo\n";
	    print IFH "$newfpo\n";
	}
        elsif($line =~ m/^\//){
	    $groupflag = 0;
	    next;
	}
    }

    #read all the parameters in the parameter group 'jobsubmit.pl'
    if ($groupflag == 2){
	if($line =~ m/^new_res*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $new_res = $line;
	    print "new_res = $new_res\n";
	}
	elsif($line =~ m/^realnumprocs*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $NPROCS = $line;
	    print "realnumprocs = $NPROCS\n";
	}
	elsif($line =~ m/^machinefile*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $NODEFILE = $line;
	    print "machinefile = $NODEFILE\n";
	}
        elsif($line =~ m/^\//){
	    $groupflag = 0;
	    next;
	}
    }

    #read the parameter MAXDISK in the parameter group 'optdec.pl'
    if ($groupflag == 3){
	if($line =~ m/^MAXDISK*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $MAXDISK = $line;
	    print "MAXDISK = $MAXDISK\n";
	}
        elsif($line =~ m/^\//){
	    $groupflag = 0;
	    next;
	}
    }
    
}
close(IFH);


#calculate and write the value of the parameter DISKBUFFER in optdec.config
open(FILE, ">$CONFIGFILE") || die "Could not write to $CONFIGFILE";

for($i = 0 ; $i < scalar(@config) ; $i++) {
    
    $line = $config[$i];
    chomp($line);
    
    if($line =~ m/^\s*DISKBUFFER*/) {

	#Remove old values of the field DISKBUFFER
	$line =~ s/=.*$/=/;

	#Calculate DISKBUFFER from $MAXDISK
	$cmd = "df -h \$WRF | tail -1 | awk '{print \$4}'";
	$output = `$cmd`;
	chomp($output);
	$avail =  substr($output, 0, length($output) - 1);

	$line .= " " . ($avail - $MAXDISK);
	print "$line\n";
    }

    print FILE "$line\n";
}
close FILE;
###############################################################


################ MAIN while loop ########################
#Intialization
$numrun = 0;
$old_res = $OLD_RES;
$MAXPROCS = $newnumprocs;
$event_flag = 0;
$first_start = 1;


#Start
while(1)
{ 
#Reset event_flag and $changeStartDate_flag
        $event_flag = 0;
	$changeStartDate_flag = 0;

#Set current time
        $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
	chomp($currtime);
	print JDBG "\njobsubmit.pl START: while(1) at $currtime\n";

	
#CHECK 1: Here the main while loop regularly checks if the WRF job has finished. If yes, then it quits the loop, bringing the entire framework to a halt.
	if(-e 'rsl.error.0000')
	{
		$cmd = "tail \-1 rsl.error.0000 | awk -F\':\' \'{print \$NF}\'";
		$retval = `$cmd`;
		if($retval =~ m/SUCCESS/)
		{
			print JDBG "\n\n$currtime JOBSUBMIT: FINALLY OVER\n";
			last;
		}
	}

#CHECK 2: Timeout will halt jobsubmit.pl and transfer.c
	if(-e 'timeout')
	{
	        print "\n$currtime JOBSUBMIT: Timing out.\n";
	        print JDBG "\n\n$currtime JOBSUBMIT: Timing out.\n";
                #Kill WRF
		kill_wrf();

	        last;
	}

#CHECK 3: Here the main while loop checks if stall file is found. This is generated by optdec.pl when it finds that the remaining disk space is below $LOWDISK threshold as specified in optdec.config configuration file.
	elsif (-e 'stall')
	{
	        print JDBG "\n$currtime: JOBSUBMIT.PL: Found stall. Shutting down WRF.";

#Kill WRF
		kill_wrf();

		while(1)
		{
#check for wakeup
			if(-e 'wakeup')	{
			    print JDBG "\n$currtime: JOBSUBMIT.PL: Wakeup found. Waking up now";
				last;
			}

#should check for timeout and come out of the loop
			if(-e 'timeout') {
				last;
			}

#now sleep check again later	
			sleep(60);	
		}

		$cmd = "rm wakeup";
		system($cmd);
		print JDBG "\n$currtime: JOBSUBMIT.PL: Deleting Wakeup";

#Set the event flag to true since wakeup event has occurred.
	        $event_flag = 1;
	}


#CHECK 4: During steering, the end-user may opt to go back in time and start the WRF simulations. 
	elsif(-e 'changeStartDate')
	{
		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
		print JDBG "\nJOBSUBMIT: $currtime: Checking for changeStartDate.\n";


                #Read changeStartDate file. The file contains parameters in the sequence $startDate, $nestXPos, $nestYPos, $simRes, $OI  
		open(CSD, "<$changeStartDateFile") || die "$changeStartDateFile open failed, $!\n";
		@changeStartDate = <CSD>;
		chomp(@changeStartDate);
		close(CSD);
		$cmd = 'mv changeStartDate changeStartDateBck';
		system($cmd);

		#Initialize variables
		($startDate, $nestXPos, $nestYPos, $simRes, $OI, $chprocs) = ($changeStartDate[0], $changeStartDate[1], $changeStartDate[2], $changeStartDate[3], $changeStartDate[4], $changeStartDate[5]);
		
		#startDate, nestXPos and nestYPos is specified
	        if(($startDate != -1) && ($nestXPos != -1) && ($nestYPos != -1))
		{
		        #Set the event flag to true since changeStartDate event has occurred.
		        $event_flag = 1;

                        #Kill WRF
			print JDBG "\n$currtime: JOBSUBMIT: Shutting down WRF. changeStartDate found.\n";
			kill_wrf();

                        #Signal transfer.c to halt all transfers to the visualization site
			$cmd = "touch donottransfer";
			system($cmd);
			print JDBG "\n$currtime: JOBSUBMIT: Stopping transfer of data to visualization site. Waiting for 300 secs.\n";
			sleep(300);

                        #Deletes all wrfoutput files
			print JDBG "\n$currtime: JOBSUBMIT: Deleting all wrf output files\n";
			$cmd = "find -name \'wrfout_d0*\' | xargs rm";
			system($cmd);

		        #Case 1: changeStartDate, nest position only
			#Case 2: changeStartDate, nest position and simulation resolution
			if((($simRes == -1) && ($OI == -1)) || (($simRes != -1) && ($OI == -1))) #Redundant check for simRes. Keeping it for readability only
			{
			        #This flag is 1 when changeStartDate event is active 
			        $changeStartDate_flag = 1;
				
				if($simRes != -1) 
				{
				        print JDBG "\n$currtime: JOBSUBMIT: User instruction to change resolution from $new_res to $simRes.\n";
				        $old_res = $new_res;
					$new_res = $simRes;
			        }
				else 
				{
				        #Both WPS and WRF have to be executed because geogrid.exe is dependent on nest position
				        $old_res = 24;  #sort of hack such that $old_res != $new_res in Check 8 and then to start_wrf()
				}

			        #Forcibly call optdec.pl
			        print JDBG "\n$currtime: JOBSUBMIT: Forcibly calling optdec.pl. Waiting 120 secs.\n";
				$cmd = "$dcm $new_res";
				system($cmd);
				sleep(120);

                                #read the oldparams file which will now have the same contents as in the change file 
				open(IFH,  "< $oldparams ") || die "$oldparams open failed: $!\n";
				@changeinput = <IFH>;
				close(IFH);

				$newHI = $changeinput[0];
				chomp($newHI);
				$newnumprocs = $changeinput[1];
				chomp($newnumprocs);
				$newfpo = $changeinput[2];
				chomp($newfpo);

				if(-e 'change')
				{
				        $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
					chomp($currtime);
					print  JDBG "\n$currtime: JOBSUBMIT: change found in the finerNest if block. Deleting change.\n";
					system("mv change changeread");
				}
    
				print JDBG "\nJOBSUBMIT: currtime=$currtime: Applying the following changes. NewHI=$newHI, procs_for_wrf.exe=$newnumprocs newfpo=$newfpo\n";

			}

			#Case 3: changeStartDate, nest position and OI/numprocs
			elsif(($simRes == -1) && (($OI != -1) || ($chprocs != -1)))
			{
			        #This flag is 1 when changeStartDate event is active 
			        $changeStartDate_flag = 1;
				
				#Both WPS and WRF have to be executed because geogrid.exe is dependent on nest position
				$old_res = 24;  #sort of hack such that $old_res != $new_res in Check 8 and then to start_wrf()

				$newHI = $OI;
				chomp($newHI);
				$newnumprocs = $chprocs;
				chomp($newnumprocs);
				$newfpo = 1;
				chomp($newfpo);

				print JDBG "\nJOBSUBMIT: currtime=$currtime: Applying the following changes. NewHI=$newHI, procs_for_wrf.exe=$newnumprocs newfpo=$newfpo\n";
			}
		}
		else
		{
		        print JDBG "\n$currtime: JOBSUBMIT: Invalid input for changing the start date. One of the parameters startDate, nestXPos and nestYPos is not provided. Ignoring...\n";
			next;
		}


	}


#CHECK 5: Whenever there is a drop in pressure value, a finerNest file is generated by WRF. This is picked up at this point. The corresponding resolution is looked up in the pressure-resolution hash array. Mostly $old_res != $new_res. The optimal decision algorithm is forcibly invoked with the new resolution. The new change and oldparams files are generated. The main while loop then detects whether there is a change in old and new resolution and then invokes either start_wrf() or restart_wrf().
	elsif (-e 'finerNest')
	{
		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
		print JDBG "\nJOBSUBMIT: $currtime: Checking for finerNest.\n";

		$old_res = $new_res;
		$new_res = &lookup('finerNest');


#remove the earlier files
		$cmd = `date +\%T`;
		my $finerbak = "finerNest".$cmd;
		`mv finerNest $finerbak`;

#Check if the resolution has changed. If yes then call optdec.pl with the new resolution and start WRF with the new parameters. Else, ignore.
		if($old_res != $new_res)
		{
#Set the event flag to true since finerNest event has occurred.
		    $event_flag = 1;

#Kill WRF
		    print JDBG "\n$currtime: JOBSUBMIT: Shutting down WRF. FinerNest found.\n";
		    kill_wrf();

	    	    print JDBG "\n$currtime: JOBSUBMIT: FinerNest found. Processing finerNest. Changing resolution from $old_res to $new_res\n";
#Forcibly call optdec.pl
		    print JDBG "\n$currtime: JOBSUBMIT: Forcibly calling optdec.pl\n";
		    $cmd = "$dcm $new_res";
		    system($cmd);
		    sleep(120);

#read the oldparams file which will now have the same contents as in the change file 
		    open(IFH,  "< $oldparams ") || die "$oldparams open failed: $!\n";
		    @changeinput = <IFH>;
		    close(IFH);

		    $newHI = $changeinput[0];
		    chomp($newHI);
		    $newnumprocs = $changeinput[1];
		    chomp($newnumprocs);
		    $newfpo = $changeinput[2];
		    chomp($newfpo);

		    if(-e 'change')
		    {
			    $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
			    chomp($currtime);
			    print  JDBG "\n$currtime: JOBSUBMIT: change found in the finerNest if block. Deleting change.\n";
			    system("mv change changeread");
		    }
    
		    print JDBG "\nJOBSUBMIT: currtime=$currtime: Applying the following changes. NewHI=$newHI, procs_for_wrf.exe=$newnumprocs newfpo=$newfpo\n";

		}
		else {
		    print JDBG "\n$currtime: JOBSUBMIT: Resolution has not changed. Ignoring finerNest.";
		    print JDBG "\nJOBSUBMIT: currtime=$currtime: Continuing with resolution $new_res, NewHI=$newHI, procs_for_wrf.exe=$newnumprocs newfpo=$newfpo\n";

		}		
	}

#CHECK 6: This if block checks if formNest file is found. This is the first time a nest is created because a drop in pressure is detected by WRF. WRF drops a file formNest with the values of i_parent_start and j_parent_start. WRF is then started with the new nest domain values.
	elsif (-e 'formNest')
	{
#Set the event flag to true since formNest event has occurred.
		$event_flag = 1;
		
#Kill WRF
		print JDBG "\n$currtime: JOBSUBMIT: Shutting down WRF. FormNest found.\n";
		kill_wrf();

		$old_res = 24;                  #sort of hack for the nest to enter the start_wrf if loop and in start_wrf

		open(NEST,  "< formNest ") || die "formNest open failed: $!\n";
		my @file = <NEST>;
		close(NEST);

		$nest_start_i = $file[1];
		chomp($nest_start_i);
		$nest_start_j = $file[2];
		chomp($nest_start_j);

		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
		print JDBG "\nJOBSUMIT: $currtime: FormNest found, so about to form new nest at $nest_start_i, $nest_start_j at res = $new_res\n";      
		`mv formNest formNestbak`;

		print JDBG "\nJOBSUBMIT: $currtime: Forcibly calling optdec.pl.\n";

		$cmd = "$dcm 24000";                    #pass new res to optdec.pl. Writes to oldparams (and change)
		system($cmd);
		sleep(120);

#read the oldparams file (whatever is the new decision by the optdec.pl)
		open(IFH,  "< $oldparams ") || die "$oldparams open failed: $!\n";
		@changeinput = <IFH>;
		close(IFH);

		$newHI = $changeinput[0];
		chomp($newHI);
		$newnumprocs = $changeinput[1];
		chomp($newnumprocs);
		$newfpo = $changeinput[2];
		chomp($newfpo);

		if(-e 'change')
		{
			$currtime = `date`;
			chomp($currtime);
			print  JDBG "\nJOBSUBMIT:$currtime: Change found. Deleting change\n";
			system("mv change changeread");
		}

		print JDBG "\nJOBSUBMIT: currtime=$currtime: Applying the following changes. NewHI=$newHI, procs_for_wrf.exe=$newnumprocs newfpo=$newfpo\n";

	}


#CHECK 7: change is a file written by optdec.pl to communicate to WRF that parameters like HI and numprocs have changed. Here the resolution does not change. Hence the main while loop will detect that the resolutionhas not changed and restart_wrf().
	elsif( -e 'change') 
	{
#Set the event flag to true since change event has occurred.
		$event_flag = 1;

#Kill WRF
		print JDBG "\n$currtime: JOBSUBMIT: Shutting down WRF.\n";
		kill_wrf();

		$currtime = `date +\%T`;
		chomp($currtime);
		print JDBG "\nJOBSUBMIT: $currtime: Change found. Going to read oldparams\n";

#read the change file
		open(IFH,  "< $oldparams ") || die "$oldparams open failed: $!\n";
		@changeinput = <IFH>;
		close(IFH);

		$newHI = $changeinput[0];
		chomp($newHI);
		$newnumprocs = $changeinput[1];
		chomp($newnumprocs);
		$newfpo = $changeinput[2];
		chomp($newfpo);

#change is read by wrf and supposed to quit
		if(-e 'change')
		{
			$currtime = `date +\%T`;
			chomp($currtime);
			print JDBG "\nJOBSUBMIT: $currtime: Change found. Deleting change\n";
			system("mv change changeread");
		}

#old_res is still the same because just external conditions have changed
		$old_res = $new_res;
		print JDBG "\nJOBSUBMIT: currtime=$currtime: Applying the following changes. NewHI=$newHI, procs_for_wrf.exe=$newnumprocs newfpo=$newfpo\n";
	}


#CHECK 8: The main while loop regularly checks for change in resolution and other parameters here. This is the main if block where the decision to either start or restart WRF is taken. If resolution is not the same (which is the case when it finds change file generated by optdec.pl ), it calls start_wrf(), else it calls restart_wrf() (in the case of formNest and finerNest).
	if($event_flag == 1 || $first_start == 1) 
	{
	    if($new_res != $old_res)
	    {
		    $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		    chomp($currtime);
		    print JDBG "\nJOBSUBMIT.pl: currtime $currtime : Beginning to start_wrf, new_res=$new_res, old_res=$old_res\n";

#Start WRF
		    print JDBG "\n$currtime: going to start wrf $numrun th time with $new_res and $old_res\n";
		    &start_wrf();

#Reset $first_start flag		  
		    $first_start = 0;
	    }
	    else
	    {
# Restart WRF
		    $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		    chomp($currtime);
		    print  JDBG "\nJOBSUBMIT:$currtime: no change in res, $new_res $old_res, going to call restart_wrf\n";

		    &restart_wrf();

		    sleep(240);
	    }
        }
	else
	{
#No event has occured. So sleep for 2 mins.
	    sleep(120);
	}
}
##################end of while(1) - The main while loop####################


$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
chomp($currtime);
close(TRH);
print JDBG "\n$currtime: BYE BYE.. EXIT\n";
exit;



########Function definitions#############################################

sub start_wrf {

	my ($cmd, $output);

	$currtime = `date +\%T`;
	chomp($currtime);
	$numrun ++;
	print JDBG "\n$currtime: start_wrf: Starting WRF Run $numrun with resolution $new_res. \n\n" ;

#if this is the first time WRF is running
	if($old_res == $OLD_RES)
	{
		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);

#Forcibly call optdec.pl at start
		print JDBG "\n$currtime: JOBSUBMIT: Forcibly calling optdec.pl at start\n";
		$cmd = "$dcm $new_res";
		system($cmd);

#read the oldparams file which will now have the same contents as in the change file 
		open(IFH,  "< $oldparams ") || die "$oldparams open failed: $!\n";
		@changeinput = <IFH>;
		close(IFH);

		$newHI = $changeinput[0];
		chomp($newHI);
		$newnumprocs = $changeinput[1];
		chomp($newnumprocs);
		$newfpo = $changeinput[2];
		chomp($newfpo);

		print JDBG "\nJOBSUBMIT: currtime=$currtime: NewHI=$newHI, maxprocs_for_wrf.exe=$newnumprocs newfpo=$newfpo machinefile=$NODEFILE, numprocs_for_real.exe= $NPROCS\n";

#Delete change file generated by optdec.pl
		if(-e 'change')
		{
			print  JDBG"\n$currtime: JOBSUBMIT: change found, deleting change\n";
			system("mv change changeread");
		}

#Read namelist.input
		open(FH, $input) or die "Cannot open file <$input> for reading";
		@inputtext = <FH>;
		close(FH);
		
#Write $newHI and $newfpo to namelist.input		
		open(FH, ">$input") or die "Cannot write to file <$input>";

		for($i = 0 ; $i < scalar(@inputtext) ; $i++) {

		    $line = $inputtext[$i];
		    chomp($line);

#replace the value of history_interval
		    if($line =~ m/^\s*history_interval/) {
#Remove old values of the field history_interval
			$line =~ s/=.*$/=/;

			$line .= floor($newHI);
			if( floor($newHI/3) ge 1) {
				$line .= ", " . floor($newHI/3) . ",";
			}
			else {
				$line .= ", 1,";
			}
		    }
#replace the value of frames_per_outfile
		    elsif($line =~ m/^\s*frames_per_outfile/) {
#Remove old values of the frames_per_outfile
			$line =~ s/=.*$/=/;

			$line .= floor($newfpo) . "," . floor($newfpo*3) . ",";
		    }

		    print FH "$line\n";
		}
		close(FH);
		print JDBG "\n$currtime: Starting WRF $numrun\n";		
	}

        #ChangeStartDate event is active
	elsif($changeStartDate_flag == 1)
	{
	        $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
		print JDBG "\n$currtime:  About to start WRF with changeStartDate event.\n";

                #Retrieve year, month, day, hour, minute, second from $startDate. This will be written to the namelist.{wps,input} files
		$timestamp = $startDate;
		#2009-05-23_06:00:00 has to be split into year, month, day, hour, minute, second
		my ($date, $time) = split(/_/, $timestamp);
		($year, $month, $day) = split(/-/, $date);
		($hour, $minute, $second) = split(/:/, $time);

                #Calculate the correct remaining time. This will be written to the namelist.{wps,input} files
		my $hoursperday = 24;
		my $remhours = ($end_day - $day) * $hoursperday + ($end_hour - $hour);
		$run_hours = $remhours % $hoursperday;
		$run_days = floor($remhours / $hoursperday);

		print JDBG "\n$currtime:  minute=$minute hour=$hour, day=$day month=$month\n";
		print JDBG "\n$currtime: remhours=$remhours, run_hours=$run_hours, run_days=$run_days";

		$dom=2;	#dirty hack

		#Set the x and y coordinates of the nest position
		$i_parent_start = $nestXPos;
		$j_parent_start = $nestYPos;
		print JDBG "\n$currtime: i_parent_start=$i_parent_start, j_parent_start=$j_parent_start";

                #parses namelist and replaces the new values
		&parse_nlinput();

                #writes into the namelist.input for WRF
		&write_nlinput();

                #same for namlist.wps, remember this is in different directory
		&parse_nlwps;
		&write_nlwps;
	}

# this is not the first time wrf is starting
	else
	{
		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
		print JDBG "\n$currtime:  Again starting WRF\n";

#to read last timestamp from output of ncdump -v Times wrfout_d01_2009-05-22_07\:30\:00
		&get_time();

		$dom=2;	#dirty hack

#to read last nest position when stopped
		&get_nestpos($new_res, $old_res);

#parses namelist and replaces the new values
		&parse_nlinput();

#writes into the namelist.input for WRF
		&write_nlinput();

#same for namlist.wps, remember this is in different directory
		&parse_nlwps;
		&write_nlwps;

		&discard_rest();
	}

	$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
	chomp($currtime);
	print  JDBG"\n$currtime: start_wrf: going to call run_wrf\n";
	&run_wrf();
}



sub restart_wrf {

       my ($cmd, $output);
       $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
       chomp($currtime);
       print JDBG "\n$currtime: restart_wrf: newres=$new_res, $old_res\n\n" ;
       $numrun ++;
       print JDBG "\n$currtime: restart_wrf:Restarting  WRF Run $numrun with resolution $new_res\n\n" ;

#to read last timestamp from output of ncdump -v Times wrfout_d01_2009-05-22_07\:30\:00
       &get_time();
       
       $cmd = "grep max_dom namelist.input | awk -F' ' '{print \$3}'";
       my $dom1 = `$cmd`;
       chomp($dom1);
       $dom = substr $dom1, 0, length($dom1) - 1;
       print "\ndom1=[$dom1] dom=[$dom]\n";

       if($dom eq 2)
       {
#to read last nest position when stopped
	       &get_nestpos($new_res, $old_res);

       }
       else
       {
#these remain the same if no nest
	       $cmd = "grep i_parent_start namelist.input | awk -F' ' '{print \$4}'";
	       my $temp = `$cmd`;
	       chomp($temp);
	       $i_parent_start=substr $temp, 0, length($temp) - 1;
	       $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
	       chomp($currtime);
	       print JDBG "\n$currtime: max_dom ne 2, temp=[$temp] i_parent_start [$i_parent_start]\n";

	       $cmd = "grep j_parent_start namelist.input | awk -F' ' '{print \$4}'";
	       $temp = `$cmd`;
	       chomp($temp);
	       $j_parent_start=substr $temp, 0, length($temp) - 1;
	       print JDBG "\n$currtime: max_dom ne 2, temp=$temp i_parent_start $i_parent_start j_parent_start $j_parent_start\n";
       }

#parses namelist and replaces the new values
       &parse_nlinput();

#writes into the namelist.input for WRF
       &write_nlinput();

#same for namlist.wps, remember this is in different directory
       &parse_nlwps;
       &write_nlwps;

#discard the rest of wrfout files which were computed with old values
       &discard_rest();

       $currtime = `date`;
       chomp($currtime);

       print JDBG "\n$currtime: restart_wrf: going to call rerun_wrf\n";
       &rerun_wrf();
}



#this function is to get year, month, day, hour, minute, second
#get the time from which to restart
sub get_time {

	print JDBG "\n$currtime: get_time function begin\n";

	my (@ncfile, $cmd, $lsmetem, @out, $line, @tokens, $i, $substr2);

	$cmd = "ls met_em* |";
	open(LSMET, $cmd) || die "ls Failed: $!\n";
	@out = <LSMET>;

	$lsmetem="";

	for($i = 0 ; $i <= $#out  ; $i++)
	{
		$line = $out[$i];
		chomp($line);
		$lsmetem = $lsmetem.$line;
	}

	close LSMET;

	$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
	chomp($currtime);
	print JDBG "\n$currtime: lsmetem=$lsmetem\n";

	@tokens=();

	my @files_d01 = <wrfout_d01*_0000>;

	print JDBG "\n\njobsubmit:$currtime:  get_time: wrfout_d01 start\n";
	print JDBG "\njobsubmit: get_time: wrfout_d01 over\n\n";	

	for($i=0;$i<$#files_d01;$i++)
	{
		chomp($files_d01[$i]);
		print TRH "\njobsubmit:$currtime: about to call savetimes fn for $files_d01[$i]\n";
		&savetimes($files_d01[$i]);
	}

#read the dumped timesteps until some boundary date is reached
	open(TIMES,  "< $timesfile ") || die "$timesfile open failed: $!\n";
	@ncfile = <TIMES>;
	close(TIMES);

	for($i = $#ncfile; $i >=0 ; $i--)
	{
		$line = $ncfile[$i];
		chomp($line);
		@tokens = split(/\"/, $line);
#Example: token[0] = 2009-05-23_12:00:09

		$substr2 = substr $tokens[0], 0, 14;
		print TRH "\njobsubmit: debug: substr2 = $substr2\n";

		if ($lsmetem =~ /$substr2/)
		{
			print TRH "\njobsubmit: debug: substr2 = $substr2 matched\n";
			print JDBG  "\njobsubmit: debug: substr2 = $substr2 matched\n";
			last;
		}

	}

#last boundary date stored in $tokens[0] -- to change namelist.wps, namelist.input
	$timestamp = $substr2."00:00";
	$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
	chomp($currtime);
	print  JDBG "\n$currtime: last boundary date: $timestamp\n";


#incase somehow the timestamp is "" then get it from namelist.wps

	if (($timestamp eq '') or ($timestamp == ""))
	{
		$cmd = "grep start_date $wps | awk -F',' '{print \$2}'";
		my $temp = `$cmd`;
		$timestamp = substr $temp, 1, length($temp) - 3;

		print  JDBG "jobsubmit: $currtime: timestamp = $timestamp\n";
	}

#2009-05-23_06:00:00 has to be split into year, month, day, hour, minute, second
	my ($date, $time) = split(/_/, $timestamp);

	($year, $month, $day) = split(/-/, $date);
	($hour, $minute, $second) = split(/:/, $time);

#to calculate the correct remaining time
	my $hoursperday = 24;
	my $remhours = ($end_day - $day) * $hoursperday + ($end_hour - $hour);

	$run_hours = $remhours % $hoursperday;
	$run_days = floor($remhours / $hoursperday);

	$currtime = `date +\%T`;	
	chomp($currtime);
	print JDBG "\n$currtime: get_time: remhours=$remhours, run_hours=$run_hours, run_days=$run_days";
	print JDBG "\n$currtime: get_time: minute=$minute hour=$hour, day=$day month=$month\n";
}


sub discard_rest
{
#if timestamp = 2009-05-23_06\:00\:00
#then discard all files from wrfout_d01_2009-05-23_06:00:00 to wrfout_d01_2009-05-23_07:30:00(ie last file)
#do ls -t wrfout_d* #take it to an array #delete upto wrfout_d01_2009-05-23_06:00:00

	my $files =  "wrfout_d01_";
	&delete($files);
	my $files =  "wrfout_d02_";
	&delete($files);
}

sub delete
{
	my $files = $_[0];
	my $subdelfiles;
	my @delfiles = `for i in $files* ; do ls \$i ;done 2> /dev/null`;
	print JDBG "$currtime: delete : Deleting all files after $files$timestamp\n";
	my $i;
	for($i = 0; $i <= $#delfiles ; $i++)
	{
		$tomatch = "$files$timestamp";

		if($files =~ m/^d0/)
		{
			$tomatch =~ s/:/_/g;
			$subdelfiles = substr($delfiles[$i], 0, 23);
		}
		else
		{
			$subdelfiles = substr($delfiles[$i], 0, 30);
		}

		if($subdelfiles ge $tomatch)
		{
			`rm $delfiles[$i]`;
		}

	}
}



sub savetimes
{
	my $i;
	$currtime = `date +\%T`;
	chomp($currtime);
	print TRH "\n$currtime: about to save time from $_[0]\n";
	my @out = grep { /2009/ } `ncdump -v Times $_[0]`;
	print TRH "\njobsubmit:$currtime: savetimes: ncdump from $_[0]\n";

	print TRH "\n$currtime: out=\n@out\n";

	if($#out lt 1)
	{
		print JDBG "\n$currtime: returning from savetimes since out length = $#out\n";
		return;
	}

	open (TFH, ">> $timesfile ") || die "$timesfile open failed: $!\n";
	for($i=0; $i<=$#out; $i++)
	{
		$out[$i] =~ s/^(\s*)//s;

		if($out[$i] =~ m/^\"/)
		{
			$out[$i] =~ s/\"//g;
			$out[$i] =~ s/\;//g;
			$out[$i] =~ s/,//g;
				$out[$i] =~ s/\s+$//;

			print TFH "$out[$i]\n";
			print JDBG "$currtime: $out[$i] printed to times.log\n";
		}
	}
	close(TFH);

}


#this function is to get $i_parent_start, j_parent_start
sub get_nestpos {

	print JDBG "\n$currtime: get_nestpos function begin\n";

	my ($icmd, $jcmd, $factor, @params);

	@params = @_;

	chomp($params[0]);      #newres
	chomp($params[1]);      #oldres

	if($new_res == 24000) {		#just to check if this is coming for the first time when the nest is being formed

		$i_parent_start = floor($nest_start_i-(112/6));
		$j_parent_start = floor($nest_start_j-(133/6));
		print JDBG "\n$currtime: Parent has found minimum at :\n$i_parent_start $j_parent_start\n";
		print JDBG "\nReturning from get_nestpos()\n";

		return;

	}

	$factor = $params[1]/$params[0];        #old/new - refer restart

	print JDBG "\nOld res = $params[1], new res = $params[0], factor = $factor\n";

	my $substr2 = substr $timestamp, 0, 14;
	print JDBG "\n$currtime: get_nestpos: [$substr2] [$timestamp]\n";

	system("cp rsl.error.0000 dump.restart");

	$icmd = "grep \"New SW corner\" dump.restart | grep \"$substr2\" | tail \-1 | awk '{print \$(NF\-1)}'";
	$jcmd = "grep \"New SW corner\" dump.restart | grep \"$substr2\" | tail \-1 | awk '{print \$NF}'";

	print JDBG "\n$currtime: icmd=$icmd\njcmd=$jcmd\n";

	$i_parent_start = `$icmd`;
	$j_parent_start = `$jcmd`;
	chomp($i_parent_start);
	chomp($j_parent_start);

	print JDBG "$currtime: I. i_parent_start = $i_parent_start, j_parent_start = $j_parent_start\n";


	if($i_parent_start == "")
	{
		$icmd = "grep \"New SW corner\" rsl.error.0000 | tail \-1 | awk '{print \$(NF\-1)}'";
		$jcmd = "grep \"New SW corner\" rsl.error.0000 | tail \-1 | awk '{print \$NF}'";

		$i_parent_start = `$icmd`;
		$j_parent_start = `$jcmd`;

		chomp($i_parent_start);
		chomp($j_parent_start);

		print JDBG "$currtime: II. i_parent_start = $i_parent_start, j_parent_start = $j_parent_start\n";
	}

#this is converting it to the new scale since now the resolution might have changed
#note:factor = 1 for restart_wrf runs

	$i_parent_start = floor($i_parent_start * $factor);
	$j_parent_start = floor($j_parent_start * $factor);

	print JDBG "$currtime: III. i_parent_start = $i_parent_start, j_parent_start = $j_parent_start\n";


	if(($i_parent_start eq 0) or ($j_parent_start eq 0))
	{
		$cmd = "grep i_parent_start namelist.input | awk -F' ' '{print \$4}'";
		my $temp = `$cmd`;
		chomp($temp);
		$i_parent_start=substr $temp, 0, length($temp) - 1;

		$cmd = "grep j_parent_start namelist.input | awk -F' ' '{print \$4}'";
		$temp = `$cmd`;
		chomp($temp);
		$j_parent_start=substr $temp, 0, length($temp) - 1;

		print JDBG "$currtime: IV. i_parent_start = $i_parent_start, j_parent_start = $j_parent_start\n";
	}
}


sub parse_nlinput {

	print JDBG "\n$currtime: parse_nlinput function begin\n";

	my ($i, $group, $line, $name, $slots, @values, %var);

	%namelist = ();

#change history_interval and frames
#read the decisionmaker scratchpad
	open(FH, $input) or die "Cannot open file <$input> for reading";
	@inputtext = <FH>;
	close(FH);

	if($new_res == 24000)
	{
	        ($parent_dx, $nest_dx, $parent_dy, $nest_dy) = (24000, 8000, 24000, 8000);
		($parent_e_we, $nest_e_we, $parent_e_sn, $nest_e_sn) = (304,97,217,127);
	}
	elsif($new_res == 21000)
	{
	        ($parent_dx, $nest_dx, $parent_dy, $nest_dy) = (21000, 7000, 21000, 7000);
		($parent_e_we, $nest_e_we, $parent_e_sn, $nest_e_sn) = (348,112,247,148);
	}
	elsif($new_res == 18000)
	{
	        ($parent_dx, $nest_dx, $parent_dy, $nest_dy) = (18000, 6000, 18000, 6000);
		($parent_e_we, $nest_e_we, $parent_e_sn, $nest_e_sn) = (406,130,288,172);
	}
	elsif($new_res == 15000)
	{
	        ($parent_dx, $nest_dx, $parent_dy, $nest_dy) = (15000, 5000, 15000, 5000);
		($parent_e_we, $nest_e_we, $parent_e_sn, $nest_e_sn) = (487,157,345,205);
	}
	elsif($new_res == 12000)
	{
	        ($parent_dx, $nest_dx, $parent_dy, $nest_dy) = (12000, 4000, 12000, 4000);
		($parent_e_we, $nest_e_we, $parent_e_sn, $nest_e_sn) = (609,196,431,256);
	}
	elsif($new_res == 10000)
	{
	        ($parent_dx, $nest_dx, $parent_dy, $nest_dy) = (10000, 3333.33, 10000, 3333.33);
		($parent_e_we, $nest_e_we, $parent_e_sn, $nest_e_sn) = (730,235,517,307);
	}
	else    {print "\nparent_dx = $parent_dx\n"; exit;}

#read and replace
	for($i = 0 ; $i < scalar(@inputtext) ; $i++)
	{
		$line = $inputtext[$i];
		strip_space_and_comment($line);
		chomp($line);

		if ($line =~ m/^&/) {
			$group = $line;
			$namelist{$group} = ();
		}
		elsif ($line =~ m/^\//) {

			$namelist{$group} = [ %var ];
		%var = ();
	    }
	    elsif ($line eq "") {
    #empty line, do nothing
	    }
	    else {
		    $line =~ s/^($regexp)\s*=\s*//s ;
		    $name = lc($1);
		    $line =~ s/\s+//g;
		    @values = split(/,/, $line);
		    $slots = -1;

		    if (@values) {
			    $slots++;
			    $var{$name} = [ @values ]; # when you assign something in square brackets, it a new reference with a new copy of the data
		    }
		    else {
			    return undef;
		    }

#replace the required variables
		    if($name =~ m/start_year/) {
			    $var{$name}[0] = $year;
			    $var{$name}[1] = $year;
		    }
		    elsif($name =~ m/start_month/) {
			    $var{$name}[0] = $month;
			    $var{$name}[1] = $month;
		    }
		    elsif($name =~ m/start_day/) {
			    $var{$name}[0] = $day;
			    $var{$name}[1] = $day;
		    }
		    elsif($name =~ m/start_hour/) {
			    $var{$name}[0] = $hour;
			    $var{$name}[1] = $hour;
		    }
		    elsif($name =~ m/start_minute/) {
			    $var{$name}[0] = $minute;
			    $var{$name}[1] = $minute;
		    }
		    elsif($name =~ m/start_second/) {
			    $var{$name}[0] = $second;
			    $var{$name}[1] = $second;
		    }
		    elsif($name =~ m/^restart$/) {
			    $var{$name}[0] = '.false.';
		    }
#this code is executed only when change in resolution is desired ie placement of nest matters
		    elsif($name =~ m/i_parent_start/) {
			    $var{$name}[1] = $i_parent_start;
		    }
		    elsif($name =~ m/j_parent_start/) {
			    $var{$name}[1] = $j_parent_start;
		    }
		    elsif($name =~ m/max_dom/) {
			    $var{$name}[0] = $dom;
		    }
		    elsif($name =~ m/^time_step$/) {
			    $var{$name}[0] = 0.003 * $parent_dx;
		    }
		    elsif($name =~ m/^dx$/) {
			    $var{$name}[0] = $parent_dx;
			    $var{$name}[1] = $nest_dx;
		    }
		    elsif($name =~ m/^dy$/) {
			    $var{$name}[0] = $parent_dy;
			    $var{$name}[1] = $nest_dy;
		    }
		    elsif($name =~ m/^e_we$/) {
			    $var{$name}[0] = $parent_e_we;
#If nest e_we  crosses the boundary of the parent domain, than limit it to parent e_we boundary
			    if($parent_e_we < ($i_parent_start + $nest_e_we/3)) {
				$nest_e_we = ($parent_e_we - $i_parent_start) * 3 + 1;
				print JDBG "\n$currtime: nest e_we crossed the boundary of the parent domain. Limiting it to parent e_we boundary\n";
			    }

			    $var{$name}[1] = $nest_e_we;
		    }
		    elsif($name =~ m/^e_sn$/) {
			    $var{$name}[0] = $parent_e_sn;

#If nest e_sn  crosses the boundary of the parent domain, than limit it to parent e_sn boundary
			    if($parent_e_sn < ($j_parent_start + $nest_e_sn/3)) {
				$nest_e_sn = ($parent_e_sn - $j_parent_start) * 3 + 1;
				print JDBG "\n$currtime: nest e_sn crossed the boundary of the parent domain. Limiting it to parent e_sn boundary\n";
			    }

    			    $var{$name}[1] = $nest_e_sn;
		    }
		    elsif($name =~ m/^run_days$/) {
			    $var{$name}[0] = $run_days;
		    }
		    elsif($name =~ m/^run_hours$/) {
			    $var{$name}[0] = $run_hours;
		    }
		    elsif($name =~ m/^history_interval$/) {
			    $var{$name}[0] = floor($newHI);
			    if( floor($newHI/3) ge 1)
			    {
				    $var{$name}[1] = floor($newHI/3);
			    }
			    else
			    {
				    $var{$name}[1] = 1;
			    }
		    }
		    elsif($name =~ m/^frames_per_outfile$/) {
			    $var{$name}[0] = floor($newfpo);
			    $var{$name}[1] = floor($newfpo*3);
		    }
		    elsif($name =~ m/^io_form_history$/) {
			    $var{$name}[0] = 102;
		    }

		} #end if else

	} #end for loop
}


sub write_nlinput {

	print JDBG "\n$currtime: write_nlinput function begin\n";

	$cmd = `date +\%T`;
	my $bak = "namelistinput".$cmd;
	`cp namelist.input $bak`;

#this function needs the global 'namelist' hash variable
	my ($group, $nl, $i);
	open(FH, ">$input") or die "Cannot open file <$input> for writing";

	my @sections = ("&time_control", "&domains", "&physics", "&fdda", "&bdy_control", "&grib2", "&namelist_quilt");

	foreach $group ( @sections ) {
		print FH "$group\n";
		my %hash = @{$namelist{$group}};
		foreach $nl ( keys %hash ) {
			print FH "$nl = ";
			foreach $i ( 0 .. $#{$hash{$nl}} ) {
				print FH "$hash{$nl}[$i], ";
			}
			print FH "\n";
		}

		print FH "/\n\n";
	}
	close(FH);
}


sub parse_nlwps {

	print JDBG "\n$currtime: parse_nlwps function begin\n";

	my ($i, $group, $line, $name, $slots, @values, %var, $iter, $flag);

	open(FH, $wps) or die "Cannot open file <$wps> for reading";
	@wpstext = <FH>;
	close(FH);

	for($i = 0 ; $i < scalar(@wpstext) ; $i++)
	{
		$line = $wpstext[$i];
		strip_space_and_comment($line);

		if ($line =~ m/^\n/) {
			$flag = 0;
			next;
		}

		chomp($line);

#Special case for press_pa <See mod_levs section in namelist.wps>
		if ($line =~ m/^\//) {
			$flag = 0;
	}

	if($flag == 1) {

		$iter++;
		$press_pa[$iter] = $line;
		next;
	}
#End of special case

	if ($line =~ m/^&/) {
		$group = $line;
		$namelistwps{$group} = ();
	}

	elsif ($line =~ m/^\//) {
		$namelistwps{$group} = [ %var ];
	%var = ();
	$flag = 0;
	}

	elsif ($line eq "") {
		$flag = 0;
#empty line, do nothing
	}

	else {
		$line =~ s/^($regexp)\s*=\s*//s ;
		$name = lc($1);

		$line =~ s/\s+//g;
		@values = split(/,/, $line);
		$slots = -1;

		if (@values) {
			$slots++;
			$var{$name} = [ @values ];
		}
		else {
			return undef;
		}

		if ($name =~ m/press_pa/) {
			$flag = 1;
			$iter = 0;
		}

		if($name =~ m/start_date/) {
			$var{$name}[0] = "\'$timestamp\'";
			$var{$name}[1] = "\'$timestamp\'";
		}
		#elsif($name =~ m/end_date/) {
		#	$var{$name}[1] = "\'$timestamp\'";
		#}
		elsif($name =~ m/max_dom/) {
			$var{$name}[0] = $dom;
		}
		elsif($name =~ m/^dx$/) {
			$var{$name}[0] = $parent_dx;
		}
		elsif($name =~ m/^dy$/) {
			$var{$name}[0] = $parent_dy;
		}
		elsif($name =~ m/^e_we$/) {
			$var{$name}[0] = $parent_e_we;
			$var{$name}[1] = $nest_e_we;
		}
		elsif($name =~ m/^e_sn$/) {
			$var{$name}[0] = $parent_e_sn;
			$var{$name}[1] = $nest_e_sn;
		}
		elsif($name =~ m/i_parent_start/) {
			$var{$name}[1] = $i_parent_start;
		}
		elsif($name =~ m/j_parent_start/) {
			$var{$name}[1] = $j_parent_start;
		}


	} #end if else

    } #end for loop
}


sub write_nlwps {

	print JDBG "\n$currtime: write_nlwps function begin\n";

	$cmd = `date +\%T`;
	my $bak = "namelistwps".$cmd;
	`cp $wps $bak`;

	my ($group, $nl, $i, $iter);
	open(FH, ">$wps") or die "Cannot open file <$wps> for writing";

	my @sections = ("&share", "&geogrid", "&ungrib", "&metgrid", "&mod_levs");

	foreach $group ( @sections ) {
		print FH "$group\n";
		my %hash = @{$namelistwps{$group}};
		foreach $nl ( keys %hash ) {
			print FH "$nl = ";

			foreach $i ( 0 .. $#{$hash{$nl}} ) {
				print FH "$hash{$nl}[$i], ";

			}
			print FH "\n";

			if ($nl =~ m/press_pa/) {
				foreach $iter ( 1 .. $#press_pa ) {
					print FH " $press_pa[$iter]\n";
				}
			}
		}
		print FH "/\n\n";
	}
	close(FH);
}


#call wps/wrf programs
sub run_wrf {

	print JDBG "\n$currtime: run_wrf function begin\n";

	my ($cmd, $path,$output);

#change to WPS dir
	my $path = $ENV{'WPS'};
	chdir($path) or die "Cant chdir to $path $!";

#remove old files
	$cmd = "rm met_em.d0* geogrid.log metgrid.log geo_em.d0*";
	`$cmd`;

#run geogrid.exe
	$cmd = "./geogrid.exe";
	`$cmd`;

	$cmd = "tail geogrid.log | grep ERROR";
	$output = `$cmd`;
	chomp($output);
	if($output == "") {
	    print JDBG "$currtime: finished executing geogrid.exe\n";
        }
	else {
	    print JDBG "$currtime: Error while executing geogrid.exe. Check geogrid.log\n";
	    die "$currtime: Error while executing geogrid.exe. Check geogrid.log\n";
	}

#run metgrid.exe
	$cmd = "./metgrid.exe";
	`$cmd`;

	$cmd = "tail metgrid.log | grep ERROR";
	$output = `$cmd`;
	chomp($output);
	if($output == "") {
	    print JDBG "$currtime: finished executing metgrid.exe\n";
        }
	else {
	    print JDBG "$currtime: Error while executing metgrid.exe. Check metgrid.log\n";
	    die "$currtime: Error while executing metgrid.exe. Check metgrid.log\n";
	}

#change back to run dir

	$path = $ENV{'WRF'};
	chdir($path) or die "Cant chdir to $path $!";

#backup rsl.error.0000
       if (-e "rsl.error.0000") {
	   $cmd = `date +\%T`;
	   my $rslbakup0 = "0000rsl.$cmd";
	   `mv rsl.error.0000 $rslbakup0`;
	}


#delete before linking new files and running real and wrf
	if (-e "rsl.error.0000") {
		print JDBG "\n$currtime: run_wrf: delete before linking new files and running real and wrf\n";
		`rm met_em.d0* namelist.output rsl.*`;
	}

#link metgrid files
	$path = $ENV{'WPS'};
	`ln -sf $path/met_em.d0* .`;

#WRF constraint: check for max number possible 
#run wrf.exe 
	print  JDBG "\n$currtime: Starting real on $NPROCS processors\n";
	$cmd = "mpirun -machinefile $NODEFILE -n $NPROCS ./real.exe";
	system($cmd);
	$cmd = `rm namelist.output rsl.*`;
	system($cmd);
	print JDBG "\n$currtime: Starting wrf on $newnumprocs processors\n";
	$cmd = "mpirun -machinefile $NODEFILE -n $newnumprocs ./wrf.exe &";
	system($cmd);
}


#call wrf programs 

sub rerun_wrf {

       print JDBG "\n$currtime: rerun_wrf function begin\n";

       my ($cmd, $path);
       print JDBG "\nJOBSUBMIT:$currtime:  restarting so no need of running WPS\n";

#backup rsl.error.0000
       if (-e "rsl.error.0000") {
          $cmd = `date +\%T`;
	  my $rslbakup0 = "0000rsl.$cmd";
	  `mv rsl.error.0000 $rslbakup0`;
       }


#delete before linking new files and running real and wrf
       if (-e "rsl.error.0000") {
	  print JDBG "\n$currtime: run_wrf: delete before linking new files and running real and wrf\n";
	  `rm met_em.d0* namelist.output rsl.*`;
       }


#link metgrid files
       $path = $ENV{'WPS'};
       `ln -sf $path/met_em.d0* .`;

#WRF constraint: check for max number possible

#run wrf.exe

       print JDBG "\n$currtime: Starting real on $MAXPROCS processors\n";
       $cmd = "mpirun -machinefile $NODEFILE -n $NPROCS ./real.exe";
       system($cmd);
       $cmd = `rm namelist.output rsl.*`;
       system($cmd);
       print JDBG "\n$currtime: Starting wrf on $newnumprocs processors\n";
       $cmd = "mpirun -machinefile $NODEFILE -n $newnumprocs ./wrf.exe &";
       system($cmd);
}


sub strip_space_and_comment {

# Strip leading space and anything from possible leading exclamation mark till the end of line.
	$_[0] =~ s/^(\s*(![^\n]*)?)*//s;
}


#trims left and right spaces
sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}


#Looks up the corresponding resolution in %pres_res hash array for a pressure value received in the FinerNest file
sub lookup()
{
	my $fileName = $_[0];
	my %pres_res = ('99500', '21000', '99100', '18000', '98900', '15000', '98700', '12000', '98500', '12000');      # check share/mediation

	print JDBG "\n$currtime:  Filename read in lookup = $fileName";

	open(FH,  "< $fileName ") || die "$fileName open failed: $!\n";
	my @file = <FH>;
	close(FH);

	chomp($file[0]);
	trim($file[0]);

	$file[0] =~ s/^\s+//; #remove leading spaces    #trim leaves 1 space in front

	print JDBG "\n$currtime: Pressure=$file[0], Res=$pres_res{$file[0]} \n";

	return $pres_res{$file[0]};
}

sub kill_wrf()
{
        my @cmd = ();

        print JDBG "\n$currtime: Going to kill WRF\n";

	@cmd = `head \-1 rsl.error.* | grep taskid  |awk \'{print \$NF}\'  |uniq`;
	print JDBG "\n$currtime: The hostnames are \n@cmd\n";

	for $i (0 .. $#cmd)
	{
	    chomp($cmd[$i]);
	    print JDBG "\n$currtime: killing wrf at hostname:$cmd[$i]\n";
	    `ssh $cmd[$i] ./killscript`;
	}
	sleep(300);		
}
