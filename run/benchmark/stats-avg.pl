#!/usr/bin/perl
#script to find timings of certain prcr nos on certain resoltuons (with nest)
use strict;

use Cwd;
my ($path, $cmd, $index);
my @prcr;
my @res = ("24", "21", "18", "15", "12");

my ($i, $j, @solve_d01, @solve_d02, @force, @feedback, @move, @sorted);
my (@wrfout_d01, @wrfout_d02, @restart_d01, @restart_d02);

#Reads the parameter processors from setup.config and assigns value to @prcr

our $regexp = qr/[a-zA-Z](?:[a-zA-Z0-9_])*/; # allowed var names
our $CONFIGFILE = '../setup.config';

print "Reading file $CONFIGFILE\n";
open(FILE, $CONFIGFILE) || die "Could not read from $CONFIGFILE";

while(<FILE>) {

    if($_ =~ m/^processors*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	@prcr = split(' ',$_);
	last;
    }
}
close FILE;

#for all res
for $j (0 .. $#res)
{
	$path = "./$res[$j]";
	chdir($path) or die "Cant chdir to $path $!";

	print "\n";

#for all processors
	for $i (0 .. $#prcr)
	{
		@solve_d01=();
		@solve_d02=();
		@force=();
		@feedback=();
		@move=();

		$path = "./$prcr[$i]";
		chdir($path) or die "Cant chdir to $path $!";

		print "$res[$j] $prcr[$i] ";

#parent
#solve_interface time
		@solve_d01 = `grep elapsed rsl.error.* | grep d01 | grep "CALL solve_interface" |  awk -F' ' '{ print \$11 }'`;

#nest
#solve_interface time
		@solve_d02 = `grep elapsed rsl.error.* | grep d02 | grep "CALL solve_interface" |  awk -F' ' '{ print \$11 }'`;


#med_nest_force
		@force = `grep elapsed rsl.error.* | grep "CALL med_nest_force" |  awk -F' ' '{ print \$11 }'`;


#med_nest_feedback
		@feedback = `grep elapsed rsl.error.* | grep "CALL med_nest_feedback" |  awk -F' ' '{ print \$11 }'`;

#med_nest_move
		`grep "CALL med_nest_move" rsl.error.* | grep "elapsed" | awk -F' ' '{print \$11}' | grep -v ^1\$  | grep -v ^0\$ > nest.$prcr[$i]`; 

#Writing wrfout
			@wrfout_d01 = `grep "Timing for Writing wrfout" rsl.error.0000 |  grep "domain        1" |  awk -F' ' '{print \$8}'`;

		@wrfout_d02 = `grep "Timing for Writing wrfout" rsl.error.0000 |  grep "domain        2" |  awk -F' ' '{print \$8}'`;

#Writing restart
		@restart_d01 = `grep "Timing for Writing restart" rsl.error.0000 |  grep "domain        1" |  awk -F' ' '{print \$8}'`;

		@restart_d02 = `grep "Timing for Writing restart" rsl.error.0000 |  grep "domain        2" |  awk -F' ' '{print \$8}'`;


#calculate the avg value of the above timings

		&avgcal(@solve_d01);
		&avgcal(@solve_d02);
		&avgcal(@force);
		&avgcal(@feedback);
		&avgcal(@wrfout_d01);
		&avgcal(@wrfout_d02);

		printf "\%6.6f \%6.6f\n", $restart_d01[0], $restart_d02[0];

		$path = "./..";
		chdir($path) or die "Cant chdir to $path $!";

	}#second for loop end

	$path = "./..";
	chdir($path) or die "Cant chdir to $path $!";

}#first for loop end

exit;


#calculate average of integer numbers
sub avgcal {

	my @array = @_;
	my $numParameters = @_;
	my @input;
	my ($total, $avg, $k) = (0, 0, 0);
	my $len = $numParameters - 1;

	for $k (0 .. $len)
	{
		chomp($array[$k]);
		$total += $array[$k];
	}

	if($len == -1)
	{
		$avg = 0;
		printf "\%6.6f ", $avg;
		return;
	}
	
	$avg = $total/($len+1);
	printf "\%6.6f ", $avg;

}


