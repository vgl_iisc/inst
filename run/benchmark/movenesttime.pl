#!/usr/bin/perl
#script to find timings of certain prcr nos on certain resolutions (with nest)
use strict;
use Cwd;

my @prcr;
my @res = ("24", "21", "18", "15", "12");

my ($i, $j, $movenest, $path, $cmd);

#Reads the parameter processors from setup.config and assigns value to @prcr

our $regexp = qr/[a-zA-Z](?:[a-zA-Z0-9_])*/; # allowed var names
our $CONFIGFILE = '../setup.config';

print "Reading file $CONFIGFILE\n";
open(FILE, $CONFIGFILE) || die "Could not read from $CONFIGFILE";

while(<FILE>) {

    if($_ =~ m/^processors*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	@prcr = split(' ',$_);
	last;
    }
}
close FILE;


#for all res
for $j (0 .. $#res)
{
	$path = "./$res[$j]";
	chdir($path) or die "Cant chdir to $path $!";

	print "\n";

#for all processors
	for $i (0 .. $#prcr)
	{

		if ($res[$j] == 24 && $prcr[$i] == 96)
		{
			next;
		}

		$path = "./$prcr[$i]";
		chdir($path) or die "Cant chdir to $path $!";

		$cmd = `awk \'{total+=\$1; count+=1} END {print total/count}\' nest.$prcr[$i]`;
		chomp($cmd);
		print "$res[$j] $prcr[$i] $cmd\n";

		$path = "./..";
		chdir($path) or die "Cant chdir to $path $!";

	}#second for loop end

	$path = "./..";
	chdir($path) or die "Cant chdir to $path $!";

}#first for loop end

exit;

