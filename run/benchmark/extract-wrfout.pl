#!/usr/bin/perl
#this file parses the output of statcollect and finds max/avg of median or avg

use strict;

my ($i, $d01, $d02);
my @res = ("24", "21", "18", "15", "12");

my $filename = 'stats-avg-output';
my $outputmin = 'wrfoutmin';

my ($max, $avg, $min);

open (MIN, ">$outputmin") || die "$outputmin open failed: $!\n";

#Writes Tio values to setup.config

our $CONFIGFILE = '../setup.config';
open(FILE,">>$CONFIGFILE") || die "Could not write to $CONFIGFILE";
print FILE "tio =";

for $i (0 .. $#res)
{
	$d02 = `grep ^$res[$i] $filename | awk -F' ' '{print \$8}' | awk '{if(min==\"\"){min=\$1}; if(\$1<min) {min=\$1};} END {print min}'`;
	$d01 = `grep ^$res[$i] $filename | awk -F' ' '{print \$7}' | awk '{if(min==\"\"){min=\$1}; if(\$1<min) {min=\$1};} END {print min}'`;

	chomp($d01);
	chomp($d02);

	print "$d01 $d02\n";
	$min = 1.000000*$d01 + 3.000000*$d02;
	print MIN "$res[$i] $min\n";

	printf FILE " %s %s", $res[$i]*1000, $min;
}

close($outputmin);
print FILE "\n";
close FILE;
