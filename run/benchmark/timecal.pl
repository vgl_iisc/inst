#!/usr/bin/perl

use strict;

my ($i, $j, $d01, $d02, $d03, $d04, $d05);
my @res = ("24", "21", "18", "15", "12");
my @prcr;

my $filename = 'stats-avg-output';
my $output = 'fisttimes';

my $avg;

#Reads the parameter processors from setup.config and assigns value to @prcr

our $regexp = qr/[a-zA-Z](?:[a-zA-Z0-9_])*/; # allowed var names
our $CONFIGFILE = '../setup.config';
our $SOLTIMEFILE = 'solvetimes'; #Intermediate file to generate solve_times in setup.config

my $minavg;

print "Reading file $CONFIGFILE\n";
open(FILE, $CONFIGFILE) || die "Could not read from $CONFIGFILE";

while(<FILE>) {

    if($_ =~ m/^processors*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	@prcr = split(' ',$_);
	last;
    }
}
close FILE;

#Open setup.config file for writing tts and solve_times
open(FILE,">>$CONFIGFILE") || die "Could not write to $CONFIGFILE";
print FILE "tts =";

#Intermediate file containing solve_times
open(STFILE,">$SOLTIMEFILE") || die "Could not write to $SOLTIMEFILE";
print STFILE "\n&solve_times\n";

open (MAX, ">$output") || die "$output open failed: $!\n";

print MAX "\nres prcr 1 2 3 4 5 total\n\n";

#@solve_d01 @solve_d02 @force @feedback @nest @total


for $i (0 .. $#res)
{
 $minavg = 0;
 printf STFILE "%s =", $res[$i]*1000;

 for $j (0 .. $#prcr)
 {	

	$d01 = `grep ^\"$res[$i] $prcr[$j]\" $filename | awk -F' ' '{print \$3}'`;
	chomp($d01);

	$d02 = `grep ^\"$res[$i] $prcr[$j]\" $filename | awk -F' ' '{print \$4}'`;
	chomp($d02);

	$d03 = `grep ^\"$res[$i] $prcr[$j]\" $filename | awk -F' ' '{print \$5}'`;
        chomp($d03);
	
	$d04 = `grep ^\"$res[$i] $prcr[$j]\" $filename | awk -F' ' '{print \$6}'`;
        chomp($d04);

	$d05 = `grep ^\"$res[$i] $prcr[$j]\" avg-nest-time | awk -F' ' '{print \$NF}'`;
	chomp($d05);

	$avg = 1.0 * $d01 +  3.0 * $d02 +  1.0 * $d03 +  1.0 * $d04 + 1.0 * $d05;	
	print MAX "$res[$i] $prcr[$j] $d01 $d02 $d03 $d04 $d05 $avg\n";

	printf STFILE " %s %s", $prcr[$j], $avg/1000.0;

	if ($minavg == 0) {
	    $minavg = $avg;
	}
	elsif ($minavg > $avg) {
	    $minavg = $avg;
	}
 }

 print MAX "\n";

 print STFILE "\n";
 printf FILE " %s %s", $res[$i]*1000, $minavg/1000.0;
}

print STFILE "/\n";
close STFILE;

#Append data from STFILE to setup.config
open(STFILE,"<$SOLTIMEFILE") || die "Could not read from $SOLTIMEFILE";

while(<STFILE>) {
    print FILE $_;
}

print FILE "\n";
close FILE;

close($output);
