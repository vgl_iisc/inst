#!/usr/bin/perl
#this file parses the output of statcollect and finds min

use strict;

my ($i, $d01, $d02, $d03, $d04);
my @res = ("24", "21", "18", "15", "12");

my $filename = 'stats-avg-output';	#input
my $outputmin = 'avg-min-time';

my $min;

open (MIN, ">$outputmin") || die "$outputmin open failed: $!\n";

#@solve_d01 @solve_d02 @force @feedback

for $i (0 .. $#res)
{
	$d01 = `grep ^$res[$i] $filename | awk -F' ' '{print \$3}' | awk '{if(min==\"\"){min=\$1}; if(\$1<min) {min=\$1};} END {print min}'`;
	chomp($d01);
	$d02 = `grep ^$res[$i] $filename | awk -F' ' '{print \$4}' | awk '{if(min==\"\"){min=\$1}; if(\$1<min) {min=\$1};} END {print min}'`;
	chomp($d02);
	$d03 = `grep ^$res[$i] $filename | awk -F' ' '{print \$5}' | awk '{if(min==\"\"){min=\$1}; if(\$1<min) {min=\$1};} END {print min}'`;
	chomp($d03);
	$d04 = `grep ^$res[$i] $filename | awk -F' ' '{print \$6}' | awk '{if(min==\"\"){min=\$1}; if(\$1<min) {min=\$1};} END {print min}'`;
	chomp($d04);

	print "$d01 $d02 $d03 $d04\n";
	$min = 1.0 * $d01 +  3.0 * $d02 +  1.0 * $d03 +  1.0 * $d04;	
	print MIN "$res[$i] $d01 $d02 $d03 $d04 $min\n";
}

close($outputmin);
