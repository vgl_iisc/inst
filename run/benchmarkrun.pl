#!/usr/bin/perl
#Runs benchmarking scripts to generate tts, tio and solved_times in setup.config

use strict;
use Switch;

print "Starting benchmarking scripts to generate tts, tio and solved_times in setup.config";

`./multipsprofiles.pl`;

chdir("./benchmark");

`./stats-avg.pl > stats-avg-output`;

`./extract.pl`;

`./extract-wrfout.pl`;

`./movenesttime.pl > avg-nest-time`;

`./timecal.pl`;

print "\nFinished executing benchmarking scripts";
