
#include <stdio.h>
#include <stdlib.h>

//Periodically calls transfer.pl to SCP the split netCDF files created, to a visualization server
int main()
{
        FILE *fp, *fpd, *fpr, *fplog;

	fplog = fopen("transferc.log","w");
	sleep(500);
	fprintf(fplog, "\nTRANSFER.c: Waiting for 500 seconds.\n");
	
        while(1) {
	        sleep(1);

		//Check for donottransfer file. This will halt the transfer process.
	        fpd = fopen("donottransfer", "r");
		if(fpd) {
		        fprintf(fplog, "\n TRANSFER.c: Found donottransfer. Waiting for resumetransfer.");
			fclose(fpd);
			system("rm donottransfer");
			fflush(fplog);

			while(1) {
			         sleep(2);
			         fpr = fopen("resumetransfer","r");
				 if(fpr) {
				        fclose(fpr);
				        system("rm resumetransfer");
				        fprintf(fplog, "\n TRANSFER.c: Found resumetransfer. Resume transfers to visualization site.");
					fflush(fplog);
					break;
				 }
			}
		}

                system("./transfer.pl");

		fp = fopen("timeout", "r");
                if(fp) {
                        //killscript should be present in the home directory
                        fprintf(fplog, "\nTRANSFER.c: found timeout\n");
                        fclose(fp);
                        break;  //break out of while
                }
        }
	fclose(fplog);
        return 0;
}


