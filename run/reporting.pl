#!/usr/bin/perl
#02-09-2010
#This program dumps wall-clock time vs simulation time in a file simwall, and disk space information in a file called diskused

use strict;
my $outputfile = 'simwall';
our $remperfile='diskused';
our $CONFIGFILE='optdec.config';
my ($currtime, $cmd, $retval);
my $MAXDISK = 130;		#mcdep
my $cmd;
my ($output, $DISKBUFFER, $avail, $remper);
our @config;
my ($line,$i, $groupflag);
our $regexp = qr/[a-zA-Z](?:[a-zA-Z0-9_])*/; # allowed var names

my $jobdbgfile = 'jobdbgfile';
open(JDBG,  ">> $jobdbgfile ") || die "$jobdbgfile open failed: $!\n";

open(OUT, ">$outputfile ") || die "$outputfile open failed: $!\n";
open(REMPER, ">$remperfile") || die "$remperfile open failed: $!\n";


print "Reading file $CONFIGFILE\n";
open(FILE, $CONFIGFILE) || die "Could not read from $CONFIGFILE";
@config = <FILE>;
close FILE;

$groupflag = 0;

for($i = 0 ; $i < scalar(@config) ; $i++) {
    
    $line = $config[$i];
    chomp($line);

    # Strip leading space and anything from possible leading exclamation mark till the end of line.
    $line =~ s/^(\s*(![^\n]*)?)*//s;

    #skip a line which has only a carriage return
    if ($line =~ m/^\n/) {
	next;
    }

    #Check if the parameter group optdec.pl exists, if yes then set flag = 1
    if ($line =~ m/^&optdec.pl/) {
	$groupflag = 1;
	print "$line\n";
	next;
    }

    #read all the parameters in the parameter group 'optdec.pl'
    if ($groupflag == 1){

	if($line =~ m/^DISKBUFFER*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $DISKBUFFER = $line;
	    print "DISKBUFFER = $DISKBUFFER\n";
	}
        elsif($line =~ m/^\//){
	    $groupflag = 0;
	    last;
	}
    }
}


#allow for initial set up of WRF
sleep(66);

while(1)
{
        $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
        chomp($currtime);

#disk space
	$cmd = "df -h \$WRF | tail -1 | awk '{print \$4}'";
	$output = `$cmd`;
	chomp($output);
	$avail =  (substr($output, 0, length($output) - 1)) - $DISKBUFFER;
	$remper = $avail * 100.0 / $MAXDISK;

	print REMPER "reporting.pl: Time= $currtime Available= $avail Remaining%= $remper\n";

#simulation rate
	if(-e 'rsl.error.0000')
	{
		$cmd = "tail \-1 rsl.error.0000 | awk -F\':\' \'{print \$NF}\'";
		$retval = `$cmd`;

		if($retval =~ m/SUCCESS/)
		{
			last;
		}

		$cmd = "tail \-2 rsl.error.0000 | awk -F\' \' \'{print \$2}\'";
		$retval = `$cmd`;
		chomp($retval);

		if($retval =~ m/^2009/)
		{
			print OUT "\n$currtime $retval";
			print JDBG "reporting.pl: $currtime $retval";
		}
		else
		{
			sleep(20);
			next;
		}
	}
	sleep(60);

}

close(OUT);
exit;

