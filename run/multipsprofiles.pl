#!/usr/bin/perl
#Performs benchmarking runs by running geogrid.exe, metgrid.exe, real.exe and wrf.exe for each resolution in @res
use strict;
use Cwd;
use POSIX;

my ($path, $cmd);
my $bench_folder = "benchmark";
my $hostfile;
my @prcr;
my @res = ("24","21","18","15","12");
my ($i, $j);
my ($d1, $d2);
my $test_res;

our $currtime;
our $regexp = qr/[a-zA-Z](?:[a-zA-Z0-9_])*/; # allowed var names
our $CONFIGFILE = 'setup.config';
our $NAMELISTWPSFILE = '../../WPS/namelist.wps.test';
our $NAMELISTINPFILE = 'namelist.input.test';
our ($run_days, $run_hours, $run_minutes, $run_seconds);
our $numprocs_real;

#Reads the parameters @prcr, $run_days, $run_hours, $run_minutes, $run_seconds and $hostfile from setup.config
print "Reading file $CONFIGFILE\n";
open(FILE, $CONFIGFILE) || die "Could not read from $CONFIGFILE";

while(<FILE>) {

    if($_ =~ m/^processors*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	@prcr = split(' ',$_);
    }
    elsif($_ =~ m/^machinefile*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	$hostfile = $_;
    }
    elsif($_ =~ m/^bench_run_days*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	$run_days = $_;
    }
    elsif($_ =~ m/^bench_run_hours*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	$run_hours = $_;
    }
    elsif($_ =~ m/^bench_run_minutes*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	$run_minutes = $_;
    }
    elsif($_ =~ m/^bench_run_seconds*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	$run_seconds = $_;
    }
    elsif($_ =~ m/^bench_numprocs_real*/) {
	chomp;
	$_ =~ s/^($regexp)\s*=\s*//s;
	$numprocs_real = $_;
    }
}
close FILE;


#Read the resolution dx or dy in namelist.wps.test on the basis of which namelist.wps files will be generated for the resolutions in @res
open(FILE, $NAMELISTWPSFILE) || die "Could not read from $NAMELISTWPSFILE";

while(<FILE>) {
    	#Match the fields dx or dy and assign to $test_res
	if( m/^\s*(dx|dy)\s*=\s*(\d+)\s*\,/) {

	    print "Test resolution is: $1 = $2";
	    $test_res = $2 / 1000;
	    last;
	}
}
close FILE;

#Create a namelist.wps for each resolution in @res
foreach $j (0 .. $#res) {

    print "\n\n\nCreating namelist.wps.$res[$j]\n";
    open(FILE, $NAMELISTWPSFILE) || die "Could not read from $NAMELISTWPSFILE";

    #Create a new namelist.wps file for the resolution $res[$j]
    open(NEWFILE, ">../../WPS/namelist.wps.$res[$j]") || die "Could not write to namelist.wps.$res[$j]";

    #Read namelist.wps.test
    while(<FILE>) {

	#Match the fields i_parent_start and j_parent_start
	if( m/^\s*(i_parent_start|j_parent_start)?\s*=\s*(\d+)\s*\,\s*(\d+)\,/) {

	    print "Matched pattern is: \n$1 = $2 and $3";

	    #Calculate the updated value for the resolution $res[$j]
	    $d1 = ceil(($3 * $test_res) / $res[$j]);

	    #Remove old values of the fields i_parent_start and j_parent_start
	    $_ =~ s/=\s*(\d+)\s*\,\s*(\d+)\,.*$/=\ $1\,/;
	    chomp;
	    #Append updated values for the resolution $res[$j]
	    $_ .= " " . $d1 . ",\n";
	    print "\n$_";

	}

	#Match the fields e_we and e_sn
	elsif( m/^\s*(e_we|e_sn)?\s*=\s*(\d+)\s*\,\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \n$1 = $2 and $3";

	    #Calculate the updated values for the resolution $res[$j]
	    $d1 = ceil(($2 * $test_res) / $res[$j]);
	    $d2 = ceil(($3 * $test_res) / $res[$j]);
	    #For nested domains, the e_we and e_sn values should be 1 greater than interger multiple of the nest's parent_grid_ratio which is assumed 3
	    $d2 = $d2 - ($d2 % 3) + 1;
    
	    #Remove old values of the fields e_we and e_sn
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated values for the resolution $res[$j]
	    $_ .= " " . $d1 . ", " . $d2 . ",\n";
	    print "\n$_";

	}

	#Match the fields dx and dy
	elsif( m/^\s*(dx|dy)\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \n$1 = $2";
	
	    #Remove old values of the field dx and dy
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value $res[$j]*1000
	    $_ .= " " . ($res[$j] * 1000) . ",\n";
	    print "\n$_";
	}

	#Match the field maxdom
	elsif( m/^\s*(max_dom)\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \n$1 = $2";
	
	    #Remove old values of the field maxdom
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value 2 (we benchmark from parent and nest domain)
	    $_ .= " 2,\n";
	    print "\n$_";
	}


    chomp;
    
        print NEWFILE "$_\n";
    }
    close FILE;
    close NEWFILE;
}



#Create a namelist.input for each resolution in @res
foreach $j (0 .. $#res) {

    print "\n\n\nCreating namelist.input.$res[$j]\n";
    open(FILE, $NAMELISTINPFILE) || die "Could not read from $NAMELISTINPFILE";

    #Create a new namelist.input file for the resolution $res[$j]
    open(NEWFILE, ">namelist.input.$res[$j]") || die "Could not write to namelist.input.$res[$j]";

    #Read namelist.input.test
    while(<FILE>) {

	#Match the fields i_parent_start and j_parent_start
	if( m/^\s*(i_parent_start|j_parent_start)?\s*=\s*(\d+)\s*\,\s*(\d+)\,/) {

	    print "Matched pattern is: \n$1 = $2 and $3";

	    #Calculate the updated value for the resolution $res[$j]
	    $d1 = ceil(($3 * $test_res) / $res[$j]);

	    #Remove old values of the fields i_parent_start and j_parent_start
	    $_ =~ s/=\s*(\d+)\s*\,\s*(\d+)\,.*$/=\ $1\,/;
	    chomp;
	    #Append updated values for the resolution $res[$j]
	    $_ .= " " . $d1 . ",\n";
	    print "\n$_";

	}

	#Match the fields e_we and e_sn
	elsif( m/^\s*(e_we|e_sn)?\s*=\s*(\d+)\s*\,\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \n$1 = $2 and $3";

	    #Calculate the updated values for the resolution $res[$j]
	    $d1 = ceil(($2 * $test_res) / $res[$j]);
	    $d2 = ceil(($3 * $test_res) / $res[$j]);

	    #For nested domains, the e_we and e_sn values should be 1 greater than interger multiple of the nest's parent_grid_ratio which is assumed 3
	    $d2 = $d2 - ($d2 % 3) + 1;

	    #Remove old values of the fields e_we and e_sn
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated values for the resolution $res[$j]
	    $_ .= " " . $d1 . ", " . $d2 . ",\n";
	    print "\n$_";

	}

	#Match the fields dx and dy
	elsif( m/^\s*(dx|dy)\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \n$1 = $2";
	
	    #Remove old values of the field max_dom
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value $res[$j]*1000
	    $_ .= " " . ($res[$j] * 1000) . ", " . (($res[$j]/3) * 1000) . ",\n";
	    print "\n$_";
	}

	#Match the field time_step
	elsif( m/^\s*time_step\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \ntime_step = $1";
	
	    #Remove old values of the field time_step
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value $res[$j]*3
	    $_ .= " " . ($res[$j] * 3) . ",\n";
	    print "\n$_";
	}
	
	#Match the field run_days
	elsif( m/^\s*run_days\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \nrun_days = $1";
	
	    #Remove old values of the field run_days
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value $run_days
	    $_ .= " " . $run_days . ",\n";
	    print "\n$_";
	}

	#Match the field run_hours
	elsif( m/^\s*run_hours\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \nrun_hours = $1";
	
	    #Remove old values of the field run_hours
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value $run_hours
	    $_ .= " " . $run_hours . ",\n";
	    print "\n$_";
	}

	#Match the field run_minutes
	elsif( m/^\s*run_minutes\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \nrun_minutes = $1";
	
	    #Remove old values of the field run_minutes
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value $run_minutes
	    $_ .= " " . $run_minutes . ",\n";
	    print "\n$_";
	}

	#Match the field run_seconds
	elsif( m/^\s*run_seconds\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \nrun_seconds = $1";
	
	    #Remove old values of the field run_seconds
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value $run_seconds
	    $_ .= " " . $run_seconds . ",\n";
	    print "\n$_";
	}
	
	#Match the field maxdom
	elsif( m/^\s*(max_dom)\s*=\s*(\d+)\s*\,/) {

	    print "Matched pattern is: \n$1 = $2";
	
	    #Remove old values of the field maxdom
	    $_ =~ s/=.*$/=/;
	    chomp;
	    #Append updated value 2 (we benchmark from parent and nest domain)
	    $_ .= " 2,\n";
	    print "\n$_";
	}

	chomp;
    
        print NEWFILE "$_\n";
    }
    close FILE;
    close NEWFILE;
}




#Start benchmarking runs
for $j (0 .. $#res)	
{
	$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
	chomp($currtime);

	print "\n$currtime: $res[$j] $prcr[$i]"; 

	$path = "../../WPS";
	chdir($path) or die "Cant chdir to $path $!";

	$cmd = "rm met_em.d0* geogrid.log metgrid.log geo_em.d0*";
	`$cmd`;

	`cp namelist.wps.$res[$j] namelist.wps`;
	`./geogrid.exe`;
	`./metgrid.exe`;

	$path = "../WRFV3/run";
	chdir($path) or die "Cant chdir to $path $!";

	$cmd = "rm met_em.d0* wrfinput* wrfbdy*";
	`$cmd`;

	`ln -sf ../../WPS/met_em.d0* .`;
	`cp namelist.input.$res[$j] namelist.input`;
	`mpirun -machinefile $hostfile -n $numprocs_real ./real.exe`;
	`rm rsl.*`;

	`mkdir $bench_folder/$res[$j]`;

	for $i (0 .. $#prcr)
	{
		`mpirun -machinefile $hostfile -n $prcr[$i] ./wrf.exe`;

		`mkdir $bench_folder/$res[$j]/$prcr[$i]`;
		`mv rsl.error* $bench_folder/$res[$j]/$prcr[$i]`;
		`echo $res[$j]/$prcr[$i] >> outputf`;
		`ls -lth wrfout*0000 >> outputf`;
		`echo "done" >> outputf`;
		`rm rsl.* wrfout* wrfrst*`;
	}

}

