#!/usr/bin/perl
#Transfers the split netCDF output files to the visualization server
 
#***************** VERY IMPORTANT *****************
#In job systems this program should only be invoked from the head node and not from the compute nodes


use strict;

our $currtime;
our (@files_d01, @files_d02);

my $tracefile = 'tracefile';
my $logfile = "transfer.log";
my $timefile = "times.log";
my $bandwidthfile = "bandwidthfile";

our $displaymc;	      
our $regexp = qr/[a-zA-Z](?:[a-zA-Z0-9_])*/; # allowed var names
our $CONFIGFILE = 'optdec.config';
our @config;
my ($line,$i, $groupflag);

#Read the parameter viz_machine from optdec.config and populate the variable $displaymc

open(FILE, $CONFIGFILE) || die "Could not read from $CONFIGFILE";
@config = <FILE>;
close FILE;

$groupflag = 0;

for($i = 0 ; $i < scalar(@config) ; $i++) {
    
    $line = $config[$i];
    chomp($line);

    # Strip leading space and anything from possible leading exclamation mark till the end of line.
    $line =~ s/^(\s*(![^\n]*)?)*//s;

    #skip a line which has only a carriage return
    if ($line =~ m/^\n/) {
	next;
    }

    #Check if the parameter group transfer.pl exists, if yes then set flag = 1
    if ($line =~ m/^&transfer.pl/) {
	$groupflag = 1;
	next;
    }

    #read all the parameters in the parameter group 'transfer.pl'
    if ($groupflag == 1){
	if($line =~ m/^viz_machine*/){
	    # retain only the value  of the parameter
            $line =~ s/^($regexp)\s*=\s*//s ;	    
	    $displaymc = $line;
	}
	elsif($line =~ m/^\//){
	    $groupflag = 0;
	    last;
	}
    }
}


open(TRH,  ">> $tracefile ") || die "$tracefile open failed: $!\n";
open (FH, ">> $logfile ") || die "$logfile open failed: $!\n";
open (TFH, ">> $timefile ") || die "$timefile open failed: $!\n";
open (BF, ">> $bandwidthfile ") || die "$bandwidthfile open failed: $!\n";

&listfiles();

close (FH);
close (TFH);
close (TRH);
close (BF);

exit;

sub listfiles {
	
	my ($i, $flag, $filename, $tarfilename, $filelist);
	#Variables for benchmarking the network bandwidth
	my ($data_size, $start_send_d1, $end_send_d1, $start_send_d2, $end_send_d2, $start_zip_d1, $end_zip_d1,  $start_zip_d2, $end_zip_d2);
	$flag = 1;

	my @file_d01 = `for i in wrfout_d01*0000 ; do ls \$i ;done 2> /dev/null`;
	my @file_d02 = `for i in wrfout_d02*0000 ; do ls \$i ;done 2> /dev/null`;

	#only if both <= 0 then return ow process
	if($#file_d01 <= 0)
	{
		if($#file_d02 <= 0)
		{
  		        sleep(2);
			print "1";
			return;
		}
	}

	#check if the nest timestamp is behind of parent
	if($#file_d02 > 0)
	{
		chomp($file_d01[0]);
		chomp($file_d02[0]);
		if($#file_d01 < 0)      #i.e no parent file present
		{
			$flag = 0;

                }
		else
		{
			if (substr($file_d02[0], 11, 16) lt substr($file_d01[0], 11, 16))
			{
			$flag = 0;
			}
		}

	}
	else
	{
		$flag = 1;
	}

	#process d01
	if($flag == 1)
	{
  	        my $substr1;

		chomp($file_d01[0]);
		$substr1 = substr ($file_d01[0], 0, length($file_d01[0]) - 4);
		@files_d01 = `ls $substr1*`;

		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
		
		#Start zip time for domain 1
		$start_zip_d1 = time();

		#Rename and zip the parent split files belonging to one timestep
		for($i = 0; $i <= $#files_d01 ; $i++)		{
                        #dump the timesteps
			chomp($files_d01[$i]);

                	#saving from *0000 is sufficient because all split files have same timestamps
			if($files_d01[$i] =~ m/0000$/)
			{
				&savetimes($files_d01[$i]);
			}
			
			$filename = $files_d01[$i];
			$filename =~ s/:/_/g;
			$filename = substr $filename, 7;
			$filename = $filename.".nc";
			`mv $files_d01[$i] $filename`;    #rename to .nc for visit
      
			#compress
			`gzip $filename`;              
			
			$tarfilename = "$filename.gz";
			$filelist = $filelist ." ". $tarfilename;
		}

		#End zip time for domain 1
		$end_zip_d1 = time();

		#Get the data size of the entire list of d01 files to be transferred
		$data_size = `ls -la $filelist | awk -F' ' '{print (\$5)}' | awk '{sum += \$1;} END {print(sum);}'`;

		#Start send time for domain 1
		$start_send_d1 = time();

		#Send all split files for one timestep and delete
		&sendfile($filelist);

		#Stop send time. All d01 files are transferred by now or overwrite it if d02 files are left..
		$end_send_d1 = time();

	}

	if($#file_d02 <= 0)		#only 1 file	#just double checking!!
	{
		print "$currtime: 2: d02 < 0 \n";
		print BF "$currtime: zip time ". ($end_zip_d1 - $start_zip_d1) ."\n";
		print BF "$currtime: sending time ". ($end_send_d1 - $start_send_d1) . " data size: ". $data_size ." \n";
		return;
	}
	chomp($file_d02[0]);
	my $substr2 = substr ($file_d02[0], 0, length($file_d02[0]) - 4);	
	@files_d02 = `ls $substr2*`;

	if(substr($file_d02[0], 11, 16) gt substr($file_d01[0], 11, 16))
	{
		print "$currtime: 3: d02 greater then d01\n";
		print BF "$currtime: zip time ". ($end_zip_d1 - $start_zip_d1) ."\n";
		print BF "$currtime: sending time ". ($end_send_d1 - $start_send_d1) . " data size: ". $data_size ." \n";
		return;
	}

	$filelist = "";

	#Start zip time for domain 2
	$start_zip_d2 = time();

        for($i = 0; $i <= $#files_d02 ; $i++)
        {
		chomp($files_d02[$i]);
		$filename = $files_d02[$i];
		$filename =~ s/:/_/g;
		$filename = substr $filename, 7;
		$filename = $filename.".nc";
		`mv $files_d02[$i] $filename`;    #rename to .nc for visit
      
		#compress
		`gzip $filename`;              
			
		$tarfilename = "$filename.gz";
		$filelist = $filelist ." ". $tarfilename;
        }

	#End zip time for domain 2
	$end_zip_d2 = time();

	#Get the data size of the entire list of d02 files to be transferred
	$data_size += `ls -la $filelist | awk -F' ' '{print (\$5)}' | awk '{sum += \$1;} END {print(sum);}'`;

	#Start send time for domain 2
	$start_send_d2 = time();

	#Send all split files for one timestep and delete
	&sendfile($filelist); 

        #Stop the benchmark time. All d02 files are transferred by now.
	$end_send_d2 = time();
	
	print BF "$currtime: zip time ". ($end_zip_d1 - $start_zip_d1) + ($end_zip_d2 - $start_zip_d2) ."\n";
	print BF "$currtime: sending time ". ($end_send_d1 - $start_send_d1) + ($end_send_d2 - $start_send_d2) . " data size: ". $data_size ." \n";
}


sub savetimes
{
        my $i;

        print TRH "\ntransfer.pl: $currtime: about to save time from $_[0]\n";

        my @out = grep { /2009/ } `ncdump -v Times $_[0]`;

        for($i=0; $i<=$#out; $i++)
        {
                $out[$i] =~ s/^(\s*)//s;

                if($out[$i] =~ m/^\"/)
                {
                        $out[$i] =~ s/\"//g;
                        $out[$i] =~ s/\;//g;
                        $out[$i] =~ s/,//g;
                        $out[$i] =~ s/\s+$//;

                        print TFH "$out[$i]\n";
                }
        }
}


sub sendfile {

	my $cmd = "scp -B  $_[0] $displaymc";

	my $retval = system($cmd);
	if($retval != 0)
	{
		print TRH "\n$currtime Transfer.pl: scp retval = $retval\n";
	}		    
	else
	{
		$currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
		chomp($currtime);
		print FH "Sent $_ at $currtime\n";
		print TRH "\nTransfer.pl: transferred $_[0] at $currtime\n";
		`rm $_[0]`; #delete split files after sending
	}
}

