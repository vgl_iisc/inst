var t, >= TLB ;
/*var z, <= 0.99 , >= LB;*/
var z, <= UB , >= LB;
var y, >= 0;

minimize
value: t;

subject to

Time: t + TIO * z = RHS * y;
/*Disk: t = FACT * z;*/
Rate: t + TIO * z <= TSBYRATE;

/*xgty: z >= y;*/

solve;

printf "%f\n", t;
printf "%f\n", z;
printf "%f\n", y;

end;
