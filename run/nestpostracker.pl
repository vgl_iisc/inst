#!/usr/bin/perl
#08-08-2011
#This program periodically tracks the nest position i.e. i_parent_start and j_parent_start and stores it in a file called nestposition

use strict;

our $trackerlog = 'nestpostracker.log';
our $nestposfile = 'nestposition';
our ($currtime, $i_parent_start, $j_parent_start, $timestamp);
my ($icmd, $jcmd, $cmd, $timecmd, $rescmd, $ret, $cur_res, $retval);

open(TL, ">> $trackerlog") || die "$trackerlog open failed: $!\n";
open(NPF, "> $nestposfile") || die "$nestposfile open failed: $!\n";


while(1)
{
        sleep(30);
        
        #Set current time
        $currtime = `date \+\"\%d\-\%m\-\%y \%T"`;
	chomp($currtime);
	
# Timeout will halt nestposttracker.pl
	if(-e 'timeout')
	{
	        print "\n$currtime JOBSUBMIT: Timing out.\n";
	        print TL "\n\n$currtime nestposttracker: Timing out.\n";
	        last;
	}

	if(-e 'rsl.error.0000')
	{
		$cmd = "cp rsl.error.0000 dump.restart";
		system($cmd);

		$cmd = "tail \-1 dump.restart | awk -F\':\' \'{print \$NF}\'";
#		$retval = `$cmd`;
		if($retval =~ m/SUCCESS/)
		{
			print TL "\n\n$currtime nestpostracker: FINALLY OVER\n";
			last;
		}
	
		#Nest X position
		$icmd = "grep \"New SW corner\" dump.restart | tail \-1 | awk '{print \$(NF\-1)}'";
		#Nest Y position
		$jcmd = "grep \"New SW corner\" dump.restart | tail \-1 | awk '{print \$NF}'";
		#timestamp
		$timecmd = "grep \"New SW corner\" dump.restart | tail \-1 | awk '{print \$2}'";
		#current resolution
		$rescmd="grep dx namelist.input | awk \-F' ' '{print \$3}'"; 

		$ret = `$rescmd`;
		$i_parent_start = `$icmd`;
		$j_parent_start = `$jcmd`;
		$timestamp = `$timecmd`;
		chomp($i_parent_start);
		chomp($j_parent_start);
		chomp($timestamp);
		chomp($ret);
		$cur_res = substr $ret, 0, length($ret) - 1;	#remove the comma at the end of $ret
	
		if(($i_parent_start eq 0) or ($j_parent_start eq 0))
		{
		        $cmd = "grep i_parent_start namelist.input | awk -F' ' '{print \$4}'";
			my $temp = `$cmd`;
			chomp($temp);
			$i_parent_start=substr $temp, 0, length($temp) - 1;

			$cmd = "grep j_parent_start namelist.input | awk -F' ' '{print \$4}'";
			$temp = `$cmd`;
			chomp($temp);
			$j_parent_start=substr $temp, 0, length($temp) - 1;

			print TL "$currtime: I. i_parent_start = $i_parent_start, j_parent_start = $j_parent_start\n";
			next;
		}
		
		elsif($i_parent_start != "")
		{
		        print TL "$currtime: timestamp = $timestamp, i_parent_start = $i_parent_start, j_parent_start = $j_parent_start, currentRes = $cur_res\n";
			print NPF "$timestamp $i_parent_start $j_parent_start $cur_res\n";
	        }
		else
		{
		        print TL "$currtime: No data!\n";		        
		}
	}
}

close(TL);
close(NPF);
exit;
